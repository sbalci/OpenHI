"""
This is the image processing module development toolbox. It contains useful functions that I have write so that the
debugging can be done easily.
"""

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from skimage import io


def imshow_cv(input_cv_img):
    # imshow area
    cv.imshow('Figure', input_cv_img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def imshow_sk(input_skimage, cv_img_yn=0):
    """
    Easy MATLAB-like imshow function just for development and debugging based on matplotlib-pyplot
    :param input_skimage: input image compatible with skimage format
    :return: (Window that displays the input image)
    """
    if cv_img_yn == 1:
        # Convert OpenCV image into Scikit Image format
        input_skimage = input_skimage[:, :, ::-1]

    fig = plt.figure("Figure")
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(input_skimage)
    plt.axis("off")
    plt.show()


