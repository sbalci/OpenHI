import os
import shutil

import openslide
import cv2 as cv
import numpy as np

from openhi.SlideManifest import SlideManifest


class Slide:
    """A class representing a whole-slide image (WSI).

    Attributes
    ----------
    slide_id: int
        Internal OpenHI slide id.
    slide_name: int
        Internal OpenHI slide id.
    file_path: str
        Full path to the slide file.
    bio_data: str
        Corresponding biospecimen data of the slide.
    clin_data: str
        Corresponding clinical data of the slide.

    """

    def __init__(self, slide_index: int, root_path: str):
        """

        :param int slide_index: The line number of the slide in the manifest file.
        :param str root_path: The root path, end with '/'.
        """

        self._root_path = root_path
        self._slide_index = slide_index
        self._manifest = SlideManifest(self._root_path + 'anno.txt')

        # TODO: Look up the slide_id from the manifest file to establish full path.
        self.slide_id = self._manifest.get_id(self._slide_index)
        self.slide_name = self._manifest.get_filename(self._slide_index)
        self.file_path = root_path + 'data/' + self.slide_id + '/' + self.slide_name

        self._oslide = openslide.open_slide(self.file_path)

        # TODO: Use the info module to extract the biospecimen and clinical data, then store them here.
        self.bio_data = None
        self.clin_data = None

    def __del__(self):
        self._oslide.close()

    def read_image(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple):
        """
                Read the original image, pslv boundary, grid or their combination.

                :param tl_coor: Coordination of top-left corner of the reading region.
                :param viewing_size: Size of the reading region.
                :param viewer_size: Required image size.
                :return: Result image in the same resolution with viewer_size.
        """
        dsr = min(viewer_size[i] / viewing_size[i] for i in range(2))
        level = self._oslide.get_best_level_for_downsample(dsr)
        reading_size = [int(viewing_size[i] / self._oslide.level_downsamples[level]) for i in range(2)]
        img = self._oslide.read_region(tl_coor, level, reading_size)
        return cv.resize(np.asarray(img), viewer_size)

    def get_size(self) -> tuple:
        """
        Get the original slide size in pixels.

        :return: Slide resolution.
        """
        return self._oslide.dimensions

    def get_pixel_size(self) -> tuple:
        """
        Get the pixel size in microns.

        :return: Pixel size (x, y).
        """
        return (float(self.get_property(openslide.PROPERTY_NAME_MPP_X)),
                float(self.get_property(openslide.PROPERTY_NAME_MPP_Y)))

    def get_property(self, key: str) -> str:
        """
        Get the property value from OpenSlide.

        :param key: Property key.
        :return: Property value.
        """
        return self._oslide.properties[key]

    def copy(self, folder_path: str) -> tuple:
        """
        Copy the svs file in to the file_path.

        :param folder_path: the place that the svs is copied to. end with '/'
        :return: file name.
        """

        folder_path = folder_path + self.slide_id + '/'
        file_name = folder_path + self.slide_name
        if not os.path.exists(folder_path):
            os.mkdir(folder_path)
        shutil.copyfile(self.file_path, file_name)

        return file_name


if __name__ == '__main__':
    pass

