# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 16:03:45 2019

@author: ZeyuGao
"""
import os

import cv2
import openslide
import numpy as np
import pandas as pd
from PIL import Image
from tqdm import tqdm

from utils import get_all_files


def gen_txt(txt_path, input_folder, label_df):
    img_list = get_all_files(input_folder)
    label_dict = {}
    for index in range(label_df.shape[0]):
        label_dict[label_df.loc[index,'filename']] = label_df.loc[index,'label']
    f = open(txt_path, 'w')
    for img_path in tqdm(img_list):
        file_name = img_path.split('/')[-2]+'.svs'
        #file_name = img_path.split('/')[-2]
        if file_name in label_dict.keys():
            label = label_dict[file_name]
            line = img_path + ' ' + '-1' + ' ' + str(label) + '\n'
            f.write(line)


def generate_binary_mask_for_wsi(file_path, output_folder):
    # 使用CV2的OTSU进行二值化
    oslide = openslide.OpenSlide(file_path)
    #    width = oslide.dimensions[0]
    #    height = oslide.dimensions[1]
    level = oslide.level_count - 1
    #    scale_down = oslide.level_downsamples[level]
    w, h = oslide.level_dimensions[level]
    # 防止出现没有放大倍数直接处理原图的情况
    if level < 1:
        print(file_path)
        oslide.close()
        return
        patch = oslide.read_region((0, 0), 0, (w, h))
        patch = patch.resize((int(w / 32), int(h / 32)), Image.ANTIALIAS)
    else:
        patch = oslide.read_region((0, 0), level, (w, h))
    slide_id = file_path.split('/')[-1].split('.svs')[0]
    patch.save('{}/{}_resized.png'.format(output_folder, slide_id));
    img = cv2.cvtColor(np.asarray(patch), cv2.COLOR_RGB2GRAY)
    img = cv2.GaussianBlur(img, (61, 61), 0)
    ret, img_filtered = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    fname = '{}/{}_mask.png'.format(output_folder, slide_id)
    cv2.imwrite(fname, img_filtered)
    oslide.close()


def normalizeStaining(img, save_file=None, img_o=240, alpha=1, beta=0.15):
    """Normalize staining appearance of H&E stained images

    Reference:
        A method for normalizing histology slides for quantitative analysis. M. Macenko et al., ISBI 2009

    :param np.ndarray img: RGB input image
    :param bool save_file: (optional) save output files
    :param float img_o: (optional) transmitted light intensity
    :param float alpha: alpha value
    :param float beta: beta value
    :return: normalizaed image
    """
    HERef = np.array([[0.5626, 0.2159],
                      [0.7201, 0.8012],
                      [0.4062, 0.5581]])

    maxCRef = np.array([1.9705, 1.0308])

    # Get image height, width, and number of channels
    h, w, c = img.shape

    # reshape image
    img = img.reshape((-1, 3))

    # calculate optical density
    OD = -np.log((img.astype(np.float) + 1) / img_o)

    # remove transparent pixels
    ODhat = OD[~np.any(OD < beta, axis=1)]

    # compute eigenvectors
    eigvals, eigvecs = np.linalg.eigh(np.cov(ODhat.T))

    # eigvecs *= -1

    # project on the plane spanned by the eigenvectors corresponding to the two
    # largest eigenvalues
    That = ODhat.dot(eigvecs[:, 1:3])

    phi = np.arctan2(That[:, 1], That[:, 0])

    minPhi = np.percentile(phi, alpha)
    maxPhi = np.percentile(phi, 100 - alpha)

    vMin = eigvecs[:, 1:3].dot(np.array([(np.cos(minPhi), np.sin(minPhi))]).T)
    vMax = eigvecs[:, 1:3].dot(np.array([(np.cos(maxPhi), np.sin(maxPhi))]).T)

    # a heuristic to make the vector corresponding to hematoxylin first and the
    # one corresponding to eosin second
    if vMin[0] > vMax[0]:
        HE = np.array((vMin[:, 0], vMax[:, 0])).T
    else:
        HE = np.array((vMax[:, 0], vMin[:, 0])).T

    # rows correspond to channels (RGB), columns to OD values
    Y = np.reshape(OD, (-1, 3)).T

    # determine concentrations of the individual stains
    C = np.linalg.lstsq(HE, Y, rcond=None)[0]

    # normalize stain concentrations
    maxC = np.array([np.percentile(C[0, :], 99), np.percentile(C[1, :], 99)])
    tmp = np.divide(maxC, maxCRef)
    C2 = np.divide(C, tmp[:, np.newaxis])

    # recreate the image using reference mixing matrix
    img_norm = np.multiply(img_o, np.exp(-HERef.dot(C2)))
    img_norm[img_norm > 255] = 254
    img_norm = np.reshape(img_norm.T, (h, w, 3)).astype(np.uint8)

    #    # unmix hematoxylin and eosin
    #    H = np.multiply(Io, np.exp(np.expand_dims(-HERef[:,0], axis=1).dot(np.expand_dims(C2[0,:], axis=0))))
    #    H[H>255] = 254
    #    H = np.reshape(H.T, (h, w, 3)).astype(np.uint8)
    #
    #    E = np.multiply(Io, np.exp(np.expand_dims(-HERef[:,1], axis=1).dot(np.expand_dims(C2[1,:], axis=0))))
    #    E[E>255] = 254
    #    E = np.reshape(E.T, (h, w, 3)).astype(np.uint8)

    if save_file is not None:
        Image.fromarray(img_norm).save(save_file)

    return img_norm


def cut_patches_from_wsi(file_path, output_folder, mask_folder, size=1000, step=500, rate=0.8, output_size=512):
    # 将wsi划窗切分成指定大小的patches
    slide_id = file_path.split('/')[-1].split('.svs')[0]
    mask_path = '{}/{}_mask.png'.format(mask_folder, slide_id)
    if not os.path.exists(mask_path):
        print("No mask file for this WSI!")
        return
    output_folder = os.path.join(output_folder, slide_id)
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    # 处理wsi
    oslide = openslide.OpenSlide(file_path)
    width = oslide.dimensions[0]
    height = oslide.dimensions[1]
    w, h = oslide.level_dimensions[-1]
    mag_w = width/w
    mag_h = height/h
    mag_size = size/mag_w
    # 读取mask图
    mask = cv2.imread('{}/{}_mask.png'.format(mask_folder, slide_id), 0)
    mask = mask.T
    if not mask.shape == (w, h):
        print("Mask file not match for this WSI!")
        return
    corrs = []
    for x in range(1, width, step):
        for y in range(1, height, step):
            if x + size > width:
                continue
            else:
                w_x = size
            if y + size > height:
                continue
            else:
                w_y = size
            # 根据mask进行过滤，大于rate个背景则不要
            mask_patch = mask[int(x/mag_w):int(x/mag_w + mag_size), int(y/mag_h):int(y/mag_h + mag_size)]
            num = mask_patch[(mask_patch == 255)].size
            if num < mask_patch.size * rate:
                corrs.append((x, y, w_x, w_y))
    print(file_path, len(corrs))
    # wsi = oslide.read_region((0, 0), 0, (width, height))
    for corr in tqdm(corrs):
        x, y, w_x, h_y = corr
        # patch = wsi.crop((x, y, x+w_x, y+h_y))
        patch = oslide.read_region((x, y), 0, (w_x, h_y))
        patch = patch.resize((output_size, output_size), Image.ANTIALIAS)
        fname = '{}/{}_{}_{}.png'.format(output_folder, x, y, size)
        patch.save(fname)
    oslide.close()


def test_gen_txt():
    input_folder = '/home1/gzy/Subtype/Patches/2000'
    label_file_path = "/home1/gzy/Subtype/SSLSupervisedData/all_ssl_crd_extension_list.txt"
    txt_path = '/home1/gzy/Subtype/SSLSupervisedData/ALL/unlabeled_2000_extension.txt'
    label_df = pd.read_csv(label_file_path)
    gen_txt(txt_path, input_folder, label_df)


def test_gen_bin_mask():
    #    input_folder = "/home1/gzy/DownloadData"
    input_folder = "/home1/pp/home_folder/docs/ccrcc_data"
    mask_folder = "/home1/gzy/Subtype/WSIMask"
    file_list = get_all_files(input_folder)
    file_list = [file for file in file_list if file.split('.')[-1] == 'svs']
    for file in tqdm(file_list):
        if '.svs' in file:
            try:
                generate_binary_mask_for_wsi(file, mask_folder)
            except Exception as e:
                print('%s: %s' % (file, e))


def test_patching():
    input_folder = "/home1/pp/home_folder/docs/ccrcc_data"
    mask_folder = "/home1/gzy/Subtype/WSIMask"
    output_folder = "/home3/gzy/Subtype/Patches/1000"
    wsi_list_path = "/home1/gzy/Subtype/labels.txt"
    wsi_df = pd.read_csv(wsi_list_path)
    wsi_list = wsi_df['filename'].tolist()
    file_list = get_all_files(input_folder)
    file_list = [file for file in file_list if file.split('.')[-1] == 'svs']
    file_list = [file for file in file_list if file.split('/')[-1] in wsi_list]
    for index in tqdm(range(200, 300)):
        file_path = file_list[index]
        cut_patches_from_wsi(file_path, output_folder, mask_folder, size=1000, step=1000, rate=0.8, output_size=512)


def test_normalization():
    input_folder = "/home1/gzy/NucleiSegmentation/CCRCC/TissueImages"
    output_folder = "/home1/gzy/NucleiSegmentation/CCRCC/NormImages"
    file_list = get_all_files(input_folder)

    for image_file in tqdm(file_list):
        save_folder = '/'.join(image_file.split('/')[:-1]).replace(input_folder, output_folder)
        if not os.path.exists(save_folder):
            os.mkdir(save_folder)
        save_file = save_folder + '/' + image_file.split('/')[-1]
        img = np.array(Image.open(image_file))[..., :3]
        print(image_file)
        normalizeStaining(img=img, save_file=save_file, img_o=240, alpha=1, beta=0.15)


if __name__ == '__main__':
    test_gen_txt()

    # -- Generate binary mask for WSI -- #
    test_gen_bin_mask()

    # -- Image normalization -- #
    test_normalization()

    # -- Patch extraction -- #
    test_patching()


