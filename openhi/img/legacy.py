"""
OpenHI Imaging Legacy APIs
--------------------------

Legacy operations  related to OpenHIv1.
"""

import configparser
import os
import numpy as np
import openhi.legacy.anno_img as img
import openhi.legacy.anno_web as web
import openhi.legacy.anno_sql as sql
import mysql.connector
import cv2 as cv


class Configuration:
    @staticmethod
    def create_connector():
        file_checking = 'module/legacy/static/OpenHI_conf.ini'
        checking_status = os.path.isfile(file_checking)
        if not checking_status:
            open_file_name = 'module/legacy/static/OpenHI_conf_example.ini'
        else:
            open_file_name = file_checking
        conf = configparser.ConfigParser()
        conf.read(open_file_name)
        conf_host = conf['db']['host']
        conf_port = conf['db']['port']
        conf_user = conf['db']['user']
        conf_passwd = conf['db']['passwd']
        conf_database = conf['db']['database']
        mydatabase = mysql.connector.connect(
            host = conf_host,
            port = conf_port,
            user = conf_user,
            passwd = conf_passwd,
            database = conf_database
        )
        return mydatabase

    @staticmethod
    def re_create_connector(mydatabase):
        """
        If the connection is timeout,then reconnect to MySQL server and database based on preset values.
        If not,keep the old connection.Return database object.

        :return: MySQL database object
        """
        try:
            mydatabase.cursor()

        except Exception:
            mydatabase.close()

            file_checking = 'module/legacy/static/OpenHI_conf.ini'
            checking_status = os.path.isfile(file_checking)

            if not checking_status:
                open_file_name = 'module/legacy/static/OpenHI_conf_example.ini'
            else:
                open_file_name = file_checking

            # Read the OpenHI INI configuration file.
            conf = configparser.ConfigParser()
            conf.read(open_file_name)

            # Read necessary information from dictionary object
            conf_host = conf['db']['host']
            conf_port = conf['db']['port']
            conf_user = conf['db']['user']
            conf_passwd = conf['db']['passwd']
            conf_database = conf['db']['database']

            # Create database object based on the given configuration
            mydatabase = mysql.connector.connect(
                host=conf_host,
                port=conf_port,
                user=conf_user,
                passwd=conf_passwd,
                database=conf_database
            )
        return mydatabase


# Internal functions
def _configuration_load():
    # Read framework configuration from INI configuration file.
    # --Check if the local configuration file exists, if not run based on the example file.
    file_checking = 'module/legacy/static/OpenHI_conf.ini'
    checking_status_conf_file = os.path.isfile(file_checking)
    if not checking_status_conf_file:
        open_file_name = 'module/legacy/static/OpenHI_conf_example.ini'
    else:
        open_file_name = file_checking

    # --Read the OpenHI INI configuration file.
    _conf = configparser.ConfigParser()
    _conf.read(open_file_name)

    return _conf


def get_original(annotator_id: int, slide_id: int, part: int) -> np.ndarray:
    """Get original image based on annotator, slide, and annotation part

    :param int annotator_id: The annotator ID in the OpenHI system.
    :param int slide_id: The Slide ID in the OpenHI system.
    :param int part: represent which part in the slide.
    :return: Original image extracted from WSI.
    :rtype: ndarray

    :Image result example:
        .. figure:: _static/original.png
           :scale: 30%

           Raw original image extracted from WSI file.
    """
    try:
        anno_id = annotator_id
        slide_id = slide_id
        tba_result = sql.get_tba_list(db, slide_id)
        total_num = len(tba_result)
        asess.init_new_annotator(anno_id, int(conf['viewer_init']['slide_id']), init_vp,
                                 Configuration.re_create_connector(db))
        conf['annotation']['manifest_filename'] = 'framework_src/manifest2_anno.txt'
        local_loader = asess.get_loader_obj(anno_id)
        list_number = part
        tba_list_location = (tba_result[list_number][1], tba_result[list_number][2])
        loc_tl_coor = (int(tba_list_location[0] - (viewing_w / 2)), int(tba_list_location[1] - (viewing_h / 2)))
        loc_viewing_size = (viewing_w, viewing_h)
        loc_viewer_size = viewer_size
        i_wsi = img.call_wsi(local_loader, loc_tl_coor, loc_viewing_size, loc_viewer_size)
        i_original = i_wsi.copy()
        return i_original
    except IndexError:
        print('Error: the max number is %d, please change the part parameter.' % total_num)


def get_original_grid(annotator_id: int, slide_id: int, part: int) -> np.ndarray:
    """Get the original image with visible grid as the annotator sees in OpenHI system.

    :param int annotator_id: The annotator ID in the OpenHI system.
    :param int slide_id: The Slide ID in the OpenHI system.
    :param int part: represent which part in the slide.
    :return: Original image extracted from WSI.
    :rtype: ndarray

    :Image result example:
        .. figure:: _static/grid.png
           :scale: 30%

           Original image with grid seen by the annotator without annotation.
    """
    smallest_allowed_grid = conf['viewer_init']['smallest_grid_size']
    i_original = get_original(annotator_id, slide_id, part)
    size_viewing = (viewing_w, viewing_h)
    size_viewer = viewer_size
    i_original_grid = img.add_grid(i_original, size_viewing, size_viewer, smallest_allowed_grid, pixel_size=0.25)
    return i_original_grid


def get_annotation(annotator_id: int, slide_id:int, part: int) -> np.ndarray:
    """Get the grading matrix with different grade on it.

    :param int annotator_id: The annotator ID in the OpenHI system.
    :param int slide_id: The Slide ID in the OpenHI system.
    :param int part: represent which part in the slide.
    :return: Original image extracted from WSI.
    :rtype: ndarray
    """
    color_scheme = [
        [0.49, 0.99, 0], [0, 1, 1], [0.54, 0.17, 0.88], [1, 0, 1], [0.82, 0.41, 0.12],
        [1, 0, 0], [0, 1, 0]
    ]
    anno_id = annotator_id
    asess.init_new_annotator(anno_id, int(conf['viewer_init']['slide_id']), init_vp, Configuration.re_create_connector(db))
    conf['annotation']['manifest_filename'] = 'framework_src/manifest2_anno.txt'
    l = asess.get_loader_obj(anno_id)
    list_number = part
    tba_result = sql.get_tba_list(db, slide_id)
    tba_list_location = (tba_result[list_number][1], tba_result[list_number][2])
    loc_tl_coor = (int(tba_list_location[0] - (viewing_w / 2)), int(tba_list_location[1] - (viewing_h / 2)))
    coor_tl = loc_tl_coor
    bw_crop = img.img_crop(l.imgBWBoun[l.current_pslv], coor_tl, size_viewing, 0)
    # Store bwboun in the loader.
    l.update_bwboun_tl(bw_crop, coor_tl)
    # Modify the masking image.
    #   Get annotated coordinate list
    point_list = sql.get_record(db, coor_tl, size_viewing, l.superpixelLv, anno_id, slide_id)

    print('Modifying layers [total = ', str(str(l.superpixelLv)), ']: ', end='')
    # Loop for the amount of pslv the current system has
    img_ff_all_level = np.zeros((size_viewing[1], size_viewing[0], 3), np.uint8)  # Prepare RGB image to store val
    # -- Prepare the n-D matrix for storing the grading.
    img_ff_level_val = np.zeros((size_viewing[1], size_viewing[0], l.maxGrading + 1), np.uint8)

    for i in range(l.superpixelLv):
        bw_crop_ff = img.img_crop(l.imgBWBoun[i], coor_tl, size_viewing, 0)
        # report current progress
        print('...', str(i), ' ', end='')
        # Get the ps level specific point list
        loop_point_list = point_list[i]

        # Generate blank image to store the masking for each grading level
        img_list_masking = list()

        for j in range(l.maxGrading):
            grading_id = j + 1
            img_bwboun_loop_temp = bw_crop_ff.copy()
            # Go through the point list and execute only the point that has the correct grading level value
            for loop_point in loop_point_list:
                # loop_coor structure: (x, y, grading)
                xy = (loop_point[0], loop_point[1])
                loop_grade = loop_point[2]  # grading_id be 1 - 5

                # Convert to local image coordinate
                input_seedpoint = tuple(np.subtract(xy, coor_tl))

                if loop_grade == grading_id:
                    cv.floodFill(img_bwboun_loop_temp, None, input_seedpoint, 255)
                else:
                    pass
            img_list_masking.append(img_bwboun_loop_temp)

        final_ff = np.zeros((size_viewing[1], size_viewing[0], 3), np.uint8)
        img_inv_boundary = cv.bitwise_not(bw_crop_ff)

        for k in range(l.maxGrading):
            # Extract the inner-sub-region (no boundaries)
            ret, bw_ff = cv.threshold(img_list_masking[k], 10, 255, cv.THRESH_BINARY)  # Create true binary image
            loop_extract = cv.bitwise_and(img_inv_boundary, bw_ff)  # Remove the boundary from flood-filled image

            # -- Blend with color and alpha
            # Make mask as color
            ff_only_mask = np.zeros_like(final_ff)
            ff_only_mask[:, :, 0] = loop_extract * color_scheme[k][0]
            ff_only_mask[:, :, 1] = loop_extract * color_scheme[k][1]
            ff_only_mask[:, :, 2] = loop_extract * color_scheme[k][2]

            final_ff = np.add(final_ff, ff_only_mask)
            # Record the specific grad1ing regions with current pslv.

            img_ff_level_val[:, :, k] = np.add(img_ff_level_val[:, :, k], loop_extract)
        img_ff_all_level = np.add(img_ff_all_level, final_ff)
    return img_ff_all_level


def get_annotation_color(annotator_id: int, slide_id: int, part: int) -> np.ndarray:
    """Get the original image with annotation as the annotator sees in OpenHI system.

    :param int annotator_id: The annotator ID in the OpenHI system.
    :param int slide_id: The Slide ID in the OpenHI system.
    :param int part: represent which part in the slide.
    :return: Original image extracted from WSI.
    :rtype: ndarray

    :Image result example:
        .. figure:: _static/annotation_color.png
           :scale: 30%

           Visualized annotation without original image.
    """
    color_scheme = [
        [0.49, 0.99, 0], [0, 1, 1], [0.54, 0.17, 0.88], [1, 0, 1], [0.82, 0.41, 0.12],
        [1, 0, 0], [0, 1, 0]
    ]
    anno_id = annotator_id
    asess.init_new_annotator(anno_id, int(conf['viewer_init']['slide_id']), init_vp, Configuration.re_create_connector(db))
    conf['annotation']['manifest_filename'] = 'framework_src/manifest2_anno.txt'
    l = asess.get_loader_obj(anno_id)
    list_number = part
    tba_result = sql.get_tba_list(db, slide_id)
    tba_list_location = (tba_result[list_number][1], tba_result[list_number][2])
    loc_tl_coor = (int(tba_list_location[0] - (viewing_w / 2)), int(tba_list_location[1] - (viewing_h / 2)))
    coor_tl = loc_tl_coor
    bw_crop = img.img_crop(l.imgBWBoun[l.current_pslv], coor_tl, size_viewing, 0)
    # Store bwboun in the loader.
    l.update_bwboun_tl(bw_crop, coor_tl)
    # Modify the masking image.
    #   Get annotated coordinate list
    point_list = sql.get_record(db, coor_tl, size_viewing, l.superpixelLv, anno_id, slide_id)

    print('Modifying layers [total = ', str(str(l.superpixelLv)), ']: ', end='')
    # Loop for the amount of pslv the current system has
    img_ff_all_level = np.zeros((size_viewing[1], size_viewing[0], 3), np.uint8)  # Prepare RGB image to store val
    # -- Prepare the n-D matrix for storing the grading.
    img_ff_level_val = np.zeros((size_viewing[1], size_viewing[0], l.maxGrading + 1), np.uint8)

    for i in range(l.superpixelLv):
        bw_crop_ff = img.img_crop(l.imgBWBoun[i], coor_tl, size_viewing, 0)
        # report current progress
        print('...', str(i), ' ', end='')
        # Get the ps level specific point list
        loop_point_list = point_list[i]

        # Generate blank image to store the masking for each grading level
        img_list_masking = list()

        for j in range(l.maxGrading):
            grading_id = j + 1
            img_bwboun_loop_temp = bw_crop_ff.copy()
            # Go through the point list and execute only the point that has the correct grading level value
            for loop_point in loop_point_list:
                # loop_coor structure: (x, y, grading)
                xy = (loop_point[0], loop_point[1])
                loop_grade = loop_point[2]  # grading_id be 1 - 5

                # Convert to local image coordinate
                input_seedpoint = tuple(np.subtract(xy, coor_tl))

                if loop_grade == grading_id:
                    cv.floodFill(img_bwboun_loop_temp, None, input_seedpoint, 255)
                else:
                    pass
            img_list_masking.append(img_bwboun_loop_temp)

        final_ff = np.zeros((size_viewing[1], size_viewing[0], 3), np.uint8)
        img_inv_boundary = cv.bitwise_not(bw_crop_ff)

        for k in range(l.maxGrading):
            # Extract the inner-sub-region (no boundaries)
            ret, bw_ff = cv.threshold(img_list_masking[k], 10, 255, cv.THRESH_BINARY)  # Create true binary image
            loop_extract = cv.bitwise_and(img_inv_boundary, bw_ff)  # Remove the boundary from flood-filled image

            # -- Blend with color and alpha
            # Make mask as color
            ff_only_mask = np.zeros_like(final_ff)
            ff_only_mask[:, :, 0] = loop_extract * color_scheme[k][0]
            ff_only_mask[:, :, 1] = loop_extract * color_scheme[k][1]
            ff_only_mask[:, :, 2] = loop_extract * color_scheme[k][2]

            final_ff = np.add(final_ff, ff_only_mask)
            # Record the specific grad1ing regions with current pslv.
            img_ff_level_val[:, :, k] = np.add(img_ff_level_val[:, :, k], loop_extract)

        # bw_img_rgb = cv.cvtColor(bw_crop_ff, cv.COLOR_GRAY2RGB)
        # final_ff_boundary = np.subtract(final_ff, bw_img_rgb)

        img_ff_all_level = np.add(img_ff_all_level, final_ff)

    # Add sub-region boundary based on current pslv
    boundary_rgb = cv.cvtColor(bw_crop, cv.COLOR_GRAY2RGB)
    final_ff_boundary = np.subtract(img_ff_all_level, boundary_rgb)

    # @pargorn: Note: 'final_ff_boundary' is the multi-color annotation image (RGB).
    #                   'boundary_rgb' is 3 layered-bw image with only the boundary.

    # Save multi-layer annotation image
    # Use 'asess' Class object to store the latest BINARY of multi-color annotation image (to further check for
    #   invalid points).
    asess.update_current_annotation(anno_id, img_ff_level_val)

    print('Flood-fill finished')

    # Resize the viewing image to viewer size so that it fits the viewer nicely.
    bw_crop_size = cv.resize(final_ff_boundary, size_viewer, interpolation=cv.INTER_LINEAR_EXACT)
    bw_bound_resize = cv.resize(bw_crop, size_viewer, interpolation=cv.INTER_LINEAR_EXACT)
    print('Image is modified')

    # Create flood-fill only region
    bw_crop_size_mask = cv.cvtColor(bw_crop_size, cv.COLOR_RGB2GRAY)
    ret, mask = cv.threshold(bw_crop_size_mask, 1, 255, cv.THRESH_BINARY)

    # ff_only_mask is the image with only annotated region (no boundary) on it.
    ff_only_mask = cv.bitwise_xor(mask, bw_bound_resize)
    ff_only_mask_inv = cv.bitwise_not(ff_only_mask)
    i_wsi = img.call_wsi(l, coor_tl, size_viewing, size_viewer)
    i_wsi_masked = cv.bitwise_and(i_wsi, i_wsi, mask=ff_only_mask_inv)

    alpha = 0.7
    beta = 1 - alpha
    img_weighted = cv.addWeighted(bw_crop_size, alpha, i_wsi, beta, 0)
    img_weighted_masked = cv.bitwise_and(img_weighted, img_weighted, mask=ff_only_mask)

    i_blend = cv.add(img_weighted_masked, i_wsi_masked)
    i_blend = cv.subtract(i_blend, cv.cvtColor(bw_bound_resize, cv.COLOR_GRAY2RGB))
    return i_blend


def get_annotation_grid(annotator_id: int, slide_id: int, part: int) -> np.ndarray:
    """Get the original image with visible grid and annotation as the annotator sees in OpenHI system.

    :param int annotator_id: The annotator ID in the OpenHI system.
    :param int slide_id: The Slide ID in the OpenHI system.
    :param int part: represent which part in the slide.
    :return: Original image extracted from WSI.
    :rtype: ndarray

    :Image result example:
        .. figure:: _static/original_grid_annotation.png
           :scale: 30%

           Original image with grid seen by the annotator with annotations.
    """
    i_annotation = get_annotation_color(annotator_id, slide_id, part)
    smallest_allowed_grid = conf['viewer_init']['smallest_grid_size']  # The size is specified in micron.
    i_annotation_grid = img.add_grid(i_annotation, size_viewing, size_viewer, smallest_allowed_grid, pixel_size=0.25)
    return i_annotation_grid


def _this_is_the_private():
    """
    Private method, it will not be included in the auto-generated documentation.

    :return:
    """
    pass


def main():
    # Define global variables:
    global viewer_size, high_mag_dim, micron_per_pixel, conf, init_vp, db, wh_ration, viewing_w, viewing_h, asess, \
        loc_viewing_size, size_viewing, loc_viewer_size, size_viewer
    viewer_size = (1000, 800)
    high_mag_dim = 250
    micron_per_pixel = 0.25
    conf = _configuration_load()
    init_vp = img.ViewingPosition()
    db = Configuration.create_connector()
    wh_ration = viewer_size[1] / viewer_size[0]
    viewing_w = int(high_mag_dim / micron_per_pixel)
    viewing_h = int(viewing_w * wh_ration)
    asess = web.AnnotatorSessionList()
    loc_viewing_size = (viewing_w, viewing_h)
    size_viewing = loc_viewing_size
    loc_viewer_size = viewer_size
    size_viewer = loc_viewer_size


if __name__ == '__main__':
    main()

