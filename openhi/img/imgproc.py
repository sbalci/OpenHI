"""
OpenHI image processing module.
"""
import os
import random

from PIL import Image
import numpy as np
import cv2 as cv

from openhi.img import legacy
from openhi.legacy import anno_sql as sql


_grade_color = [[124, 252, 0], [0, 255, 255], [137, 43, 224], [255, 0, 255]]


def _get_grade(color: np.ndarray):
    for i in range(len(_grade_color)):
        if (color == _grade_color[i]).all():
            return i
    return -1


def image_split(image, mode, path=None, write=False):
    """Split input image into m * n pixels pieces.

    :param string image: The path of input image
    :param tuple mode: size of sub-rectangle region, tuple: (m, n)
    :param string path: Output path
    :param bool write: Save sub-images into a file or not
    :return: a list of sub-images, base coordinates of sub-images (left-top coordinates)
    :rtype: list
    """
    im = Image.open(image)  # read input image
    size = im.size  # get the size of the image
    (m, n) = mode  # get the size of the sub-images
    col = size[0] // m
    row = size[1] // n
    subimage_num = col * row  # number of sub-images
    subimage_list = [None] * subimage_num
    base_coordinate = [None] * subimage_num
    left_margin = round((size[0] - col * m) / 2)
    top_margin = round((size[1] - row * n) / 2)
    base_coordinate[0] = (left_margin, top_margin)
    # get base coordinate for each sub-image
    for i in range(1, subimage_num):
        x = left_margin + (i % col) * m
        y = top_margin + (i // row) * n
        base_coordinate[i] = (x, y)

    for i in range(subimage_num):
        base = base_coordinate[i]
        box = (base[0], base[1], base[0]+m, base[1]+n)
        subimage_list[i] = im.crop(box)

    if write is True:
        for i in range(subimage_num):
            filename = str(i) + ".png"
            subimage_list[i].save(path + filename)

    return subimage_list, base_coordinate


def read_annotation(fp):
    """Read the annotated data into a list.

    :param string fp: File path
    :return: A list containing tuples: (image NO., coordinate x, coordinate y, grade)
    :rtype: list
    """
    file = open(fp)
    readlines = file.readlines()
    readlines = [item.strip('\n') for item in readlines]
    readlines = [item.split(',') for item in readlines]
    for i in range(len(readlines)):
        readlines[i] = [eval(item) for item in readlines[i]]
        readlines[i] = tuple(readlines[i])
    return readlines


def fetch_coordinate(image_num, annotation):
    """Fetch coordinates of nuclei in specified image, given annotation of all images.

    :param int image_num: index of specified image
    :param list annotation: annotation information of all images
    :return: coordinates of nuclei in specified image
    :rtype: list
    """
    result = []
    for item in annotation:
        if image_num == item[0]:
            result += [(item[1], item[2])]
    return result


def convert_coordinate(benchmark, boxsize, coordinate):
    """Convert coordinates of nuclei based on the benchmark and boxsize of sub-image.

    :param tuple benchmark: left top coordinate of sub-image
    :param tuple boxsize: size of sub-image
    :param list coordinate: coordinates of all nuclei in original image
    :return: converted coordiantes
    :rtype: list
    """
    result = []
    for item in coordinate:
        if benchmark[0] <= item[0] <= benchmark[0]+boxsize[0]:
            if benchmark[1] <= item[1] <= benchmark[1]+boxsize[1]:
                result.append((coordinate[0]-benchmark[0], coordinate[1]-benchmark[1]))
    return result


def gen_path_sliding_window(patch_size, path_patches):
    """Generate the patch in a sliding window fashion.

    This function can also be used to generate the tile (no overlapping regions) by setting the step size to zero.

    :param: int patch_size: the size of patch.
    :param: String path_patches: txt file's path which saves the path of patches and label.
    :return:
    :rtype: str
    """
    annotators_id = [1, 2]  # represent two annotators
    slides_id = [1]  # represent slide_id
    db = legacy.Configuration.create_connector()

    # some parameters
    step = patch_size // 2
    num = 2
    ws = patch_size // num
    if not os.path.exists(path_patches):
        os.mkdir(path_patches)
    file_path = os.path.join(path_patches, 'patch_labels')
    pl_file = open(file_path, 'w')
    grades_num = [0, 0, 0, 0, 0]  # represent each grade having how many patches.

    for anno_id in annotators_id:
        for slide_id in slides_id:  # through slide_id we can get we have how many parts in database.
            tba_result = sql.get_tba_list(db, slide_id)
            image_nums = len(tba_result)
            index = 0
            for part in range(image_nums):
                orin_image = legacy.get_original(anno_id, slide_id, part)
                anno_image = legacy.get_annotation(anno_id, slide_id, part)
                size_y, size_x = orin_image.shape[0], orin_image.shape[1]
                nx = (size_x - patch_size) // step + 1
                ny = (size_y - patch_size) // step + 1
                patch_coord = np.ndarray(shape=(nx*ny, 2), dtype=np.int32)
                i = 0
                for y in range(0, size_y-patch_size, step):
                    for x in range(0, size_x-patch_size, step):
                        patch_coord[i] = [x, y]   # the coordinate of left-top point of each patch.
                        i += 1
                for j, (i_0, j_0) in enumerate(patch_coord):
                    orin_patch = orin_image[j_0:j_0 + patch_size, i_0:i_0 + patch_size]
                    anno_patch = anno_image[j_0:j_0 + patch_size, i_0:i_0 + patch_size]
                    central_grades = anno_patch[((patch_size - ws) // 2):((patch_size + ws) // 2),
                                     ((patch_size - ws) // 2):((patch_size + ws) // 2)]

                    de_central_grades0 = np.array(list(set([tuple(t) for t in anno_patch.reshape(-1, 3)])))

                    grade_num = de_central_grades0.shape[0]
                    if grade_num == 1 and np.all(de_central_grades0[0] == [0, 0, 0]):
                        grade = 0
                    else:
                        de_central_grades = np.array(list(set([tuple(t) for t in central_grades.reshape(-1, 3)])))
                        grade_num = de_central_grades.shape[0]
                        if grade_num == 2:
                            if np.all(de_central_grades[0] == [0, 0, 0]):
                                pixel_value = de_central_grades[1]
                                if np.all(pixel_value == [0, 0, 0]):
                                    grade = -1
                                elif np.all(pixel_value == [124, 252, 0]):
                                    grade = 1
                                elif np.all(pixel_value == [0, 255, 255]):
                                    grade = 2
                                elif np.all(pixel_value == [137, 43, 224]):
                                    grade = 3
                                else:
                                    grade = -1
                            else:
                                pixel_value = de_central_grades[0]
                                if np.all(pixel_value == [0, 0, 0]):
                                    grade = -1
                                elif np.all(pixel_value == [124, 252, 0]):
                                    grade = 1
                                elif np.all(pixel_value == [0, 255, 255]):
                                    grade = 2
                                elif np.all(pixel_value == [137, 43, 224]):
                                    grade = 3
                                else:
                                    grade = -1
                        elif grade_num == 1:
                            pixel_value = de_central_grades[0]
                            if np.all(pixel_value == [0, 0, 0]):
                                grade = -1
                            elif np.all(pixel_value == [124, 252, 0]):
                                grade = 1
                            elif np.all(pixel_value == [0, 255, 255]):
                                grade = 2
                            elif np.all(pixel_value == [137, 43, 224]):
                                grade = 3
                            else:
                                grade = -1
                        else:
                            grade = -1

                    if grade != -1:
                        grades_num[grade] += 1
                        orin_image_name = 'orin_' + 'a%ds%dp%dp%d' % (anno_id, slide_id, part, index) + '.png'
                        orin_image_path = os.path.join(path_patches.split('/')[-1], orin_image_name)
                        temp = orin_image_path + '  ' + str(grade) + '\n'
                        index += 1
                        pl_file.write(temp)
                        orin_image_path = os.path.join(os.getcwd() + '/openhi/img/', orin_image_path)
                        image = Image.fromarray(orin_patch)
                        image.save(orin_image_path )
    print('\n')
    for gn in range(5):
        print('grade %d: %d' % (gn, grades_num[gn]))
        print('grade %d: %d' % (gn, grades_num[gn]))


def gen_path_annotation_based(orin_img_lst: [], anno_img_lst: [], output_dir: str, patch_size: int = 64,
                              overlap_size: int = 32, overlap_size_zero: int = 40, core_size: int = 16,
                              core_ratio: float = 0.5, core_conflict_ratio: float = 0.1,
                              conflict_ratio: float = 0.2, conflict_ratio_zero: float = 0.1,
                              stride: int = 1, dataset_size: int = 1000, debug: bool = False, print_log: bool = True):
    """Generate the patch based on annotation region.

    :param orin_img_lst: Original whole-slide image list.
    :param anno_img_lst: Generated annotation image list.
    :param output_dir: Directory to put generated patches.
    :param patch_size: Size of each patch.
    :param overlap_size: Any two of the generated patches will at least at a distance of overlap_size.
    :param overlap_size_zero: overlap_size for grade 0.
    :param core_size: Used to determine the grade of each patch.
    :param core_ratio: If more than core_ratio of the pixels in the core are of the same grade, a patch is generated.
    :param core_conflict_ratio: Each patch core must have more than core_conflict_ratio of all pixels with the same grade.
    :param conflict_ratio: Each patch must have more than conflict_ratio of all pixels with the same grade.
    :param conflict_ratio_zero: conflict_ratio for grade 0.
    :param stride: Stride for sliding window.
    :param dataset_size: Number of patch files to generate.
    :param debug: Draw patch locations on anno_img.
    :param print_log: Print log to standard output.
    :return: The number of generated patches of each grade.
    :rtype: int
    """
    patchs = []
    patch_start = 0
    res_patchs = [[], [], []]
    train_labels = open(output_dir + '/train_labels.txt', 'w')
    test_labels = open(output_dir + '/test_labels.txt', 'w')

    for t in range(len(orin_img_lst)):
        orin_img = orin_img_lst[t]
        anno_img = anno_img_lst[t]

        if print_log:
            print('generating patch for image %d.' % t)

        prefix_sum = np.zeros([orin_img.shape[0] + 1, orin_img.shape[1] + 1, 4], dtype=np.int32)
        for i in range(orin_img.shape[0]):
            for j in range(orin_img.shape[1]):
                _grade = _get_grade(anno_img[i][j])
                _value = np.zeros([4], dtype=np.int32)
                if _grade >= 0:
                    _value[_grade] = 1
                _value += prefix_sum[i + 1][j] + prefix_sum[i][j + 1] - prefix_sum[i][j]
                prefix_sum[i + 1][j + 1] = _value
            if i % 100 == 0 and print_log:
                print('preprocessing %.0f%%.' % (i / orin_img.shape[0] * 100))

        if print_log:
            print('preprocessing complete.')

        patch_size_2 = patch_size // 2
        core_size_2 = core_size // 2
        patch_area = patch_size * patch_size
        core_area = core_size * core_size
        core_mincnt = core_area * core_ratio
        core_conflict_area = core_area * core_conflict_ratio
        overlap_area = overlap_size * overlap_size
        overlap_area_zero = overlap_size_zero * overlap_size_zero

        def _region_sum(x1, y1, x2, y2):
            return prefix_sum[y2][x2] + prefix_sum[y1][x1] - prefix_sum[y2][x1] - prefix_sum[y1][x2]

        def _get_max_grade(value: np.ndarray):
            grade = np.argmax(value)
            return grade if value[grade] > 0 else -1

        def _region_check(x, y):
            if x + patch_size_2 >= orin_img.shape[1] or x - patch_size_2 < 0\
                    or y + patch_size_2 >= orin_img.shape[0] or y - patch_size_2 < 0:
                return -2

            value = _region_sum(x - core_size_2, y - core_size_2, x + core_size_2, y + core_size_2)
            grade = _get_max_grade(value)
            if grade >= 0:
                if np.sum(value) - value[grade] > core_conflict_area:
                    return -2
                if value[grade] < core_mincnt:
                    grade = -1

            value = _region_sum(x - patch_size_2, y - patch_size_2, x + patch_size_2, y + patch_size_2)
            value_sum = np.sum(value)
            if grade == -1:
                return -1 if value_sum <= patch_area * conflict_ratio_zero else -2

            if value_sum - value[grade] > conflict_ratio * value_sum:
                return -2
            return grade

        def _patch_overlap(a, b):
            dx = abs(a[0][0] - b[0][0])
            dy = abs(a[0][1] - b[0][1])
            if a[1] == -1 or b[1] == -1:
                return dx * dx + dy * dy < overlap_area_zero
            return dx * dx + dy * dy < overlap_area

        def _patch_insert(p):
            x1 = p[0][0] - patch_size_2
            x2 = x1 + patch_size
            y1 = p[0][1] - patch_size_2
            y2 = y1 + patch_size
            grade = p[1]
            if grade >= 0:
                for i in range(patch_size_2):
                    p[2] += _region_sum(x1, y1, x2, y2)[grade]
                    x1 += 1
                    x2 -= 1
                    y1 += 1
                    y2 -= 1
            else:
                value = _region_sum(x1, y1, x2, y2)
                p[2] = patch_area - np.sum(value)

            rep = -1
            cnt = [0, 0]
            for i in range(patch_start, len(patchs)):
                if _patch_overlap(patchs[i], p):
                    if p[2] > patchs[i][2] or (p[1] >= 0 and patchs[i][1] < 0):
                        rep = i
                        cnt[1] += 1
                    else:
                        cnt[0] += 1

            if cnt[0] == 0 and cnt[1] < 2:
                if cnt[1] == 1:
                    patchs[rep] = p
                else:
                    patchs.append(p)

        for x in range(0, orin_img.shape[1], stride):
            for y in range(0, orin_img.shape[0], stride):
                grade = _region_check(x, y)
                if grade >= -1:
                    _patch_insert([[x, y], grade, 0])
                    while patch_start < len(patchs) and patchs[patch_start][0][0] + overlap_size < x:
                        patch_start += 1
            if x % 100 == 0 and print_log:
                print('processing stage 1 %.0f%%, patch count %d.' % (x / orin_img.shape[1] * 100, len(patchs)))

        patchs.reverse()
        patch_start = 0
        if print_log:
            print('stage 1 complete.')

        for x in range(orin_img.shape[1] - 1, -1, -stride):
            for y in range(orin_img.shape[0] - 1, -1, -stride):
                grade = _region_check(x, y)
                if grade >= -1:
                    _patch_insert([[x, y], grade, 0])
                    while patch_start < len(patchs) and patchs[patch_start][0][0] - overlap_size > x:
                        patch_start += 1
            if x % 100 == 0 and print_log:
                print('processing stage 2 %.0f%%, patch count %d.' % ((1 - x / orin_img.shape[1]) * 100, len(patchs)))

        if print_log:
            print('stage 2 complete.')

        counter = 0
        for i in range(len(patchs)):
            p = patchs[i]
            roi = orin_img[p[0][1] - patch_size_2:p[0][1] + patch_size_2, p[0][0] - patch_size_2:p[0][0] + patch_size_2, :]

            rcnt = 1 if p[1] == 1 else 4
            for r in range(rcnt):
                filename = 'patch_%d.png' % counter
                counter += 1
                cv.imwrite(output_dir + '/' + filename, roi)
                if rcnt > 1:
                    cv.rotate(roi, cv.ROTATE_90_CLOCKWISE, roi)
                if p[1] < 2:
                    res_patchs[p[1] + 1].append(filename)

            if debug:
                cv.rectangle(anno_img, (p[0][0] - patch_size_2, p[0][1] - patch_size_2), (p[0][0] + patch_size_2, p[0][1]
                             + patch_size_2), (0, 0, 255) if p[1] >= 0 else (255, 0, 0))

            if counter % 500 == 0 and print_log:
                print('writing patch files %.0f%%.' % (i / len(patchs) * 100))

    if print_log:
        print('generating patch labels.')

    dataset_size //= 3
    for g in range(3):
        dataset_size = min(dataset_size, len(res_patchs[g]))
    test_size = dataset_size // 10
    train_size = dataset_size - test_size
    for g in range(3):
        random.shuffle(res_patchs[g])
        for i in range(train_size):
            train_labels.write('%s %d\n' % (res_patchs[g][i], g + 1))
        for i in range(train_size, train_size + test_size):
            test_labels.write('%s %d\n' % (res_patchs[g][i], g + 1))
        for i in range(train_size + test_size, len(res_patchs[g])):
            os.remove(output_dir + '/' + res_patchs[g][i])

    train_labels.close()
    test_labels.close()

    if print_log:
        print('patch labels saved.')

    return dataset_size * 3
