'''
@Date: 2020-02-28 20:42:21
@LastEditors: JiangBo Shi
@LastEditTime: 2020-03-02 23:01:14
@FilePath: /histopathology_feature_extraction/utils.py
@Author: JiangBo Shi

Note: this file is for `feature.py`
'''
import os
import random

import numpy as np
import pandas as pd
import cv2 as cv
from skimage.color import rgb2hed
from skimage import color
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from itertools import groupby
from tqdm import tqdm


def read_image(image_path):   
    img = cv.imread(image_path)
    img = cv.cvtColor(img, cv.COLOR_BGR2RGB) # cv2默认为bgr顺序
    return img


# 将txt文件中的数据读入numpy数组
def read_txt(file_path):
    return np.loadtxt(file_path, dtype=int, delimiter=',')


def Hemo_from_HE(image_path):
    image = read_image(image_path)
    return get_H_channel(image)


def V_from_HSV(image_path):
    image = read_image(image_path)
    hsv = color.rgb2hsv(image)
    V = hsv[:, :, 0]
    return V


def L_from_Lab(image_path):
    image = read_image(image_path)
    lab = color.rgb2lab(image)
    L = lab[:, :, 0]
    return L


def show_H_channel(image):
    # Create an artificial color close to the original one
    cmap_hema = LinearSegmentedColormap.from_list('mycmap', ['white', 'navy'])
    ihc_rgb = image
    ihc_hed = rgb2hed(ihc_rgb)
    fig, axes = plt.subplots(1, 2, figsize=(7, 6), sharex=True, sharey=True)
    ax = axes.ravel()
    ax[0].imshow(ihc_rgb)
    ax[0].set_title("Original image")
    ax[1].imshow(ihc_hed[:, :, 0], cmap=cmap_hema)
    ax[1].set_title("Hematoxylin")
    for a in ax.ravel():
        a.axis('off')
    fig.tight_layout()
    plt.show()


def get_H_channel(image):
    return rgb2hed(image)[:, :, 0]


# 将所有的bounrary.txt中的数字修改成0、1的形式。1表示细胞核，0表示背景。
def image_boundary_normalization(file):
    file[file == 1] = 0
    file[file == -1] = 0
    file[file != 0] = 1
    return file    


def getGrayLevelRumatrix(array, theta):
    """计算给定图像的灰度游程矩阵

    参数：
    array: 输入，需要计算的图像
    theta: 输入，计算灰度游程矩阵时采用的角度，list类型，可包含字段:['deg0', 'deg45', 'deg90', 'deg135']
    glrlm: 输出，灰度游程矩阵的计算结果
    """
    P = array
    x, y = P.shape
    min_pixels = np.min(P)   # 图像中最小的像素值
    run_length = max(x, y)   # 像素的最大游行长度
    num_level = np.max(P) - np.min(P) + 1   # 图像的灰度级数

    deg0 = [val.tolist() for sublist in np.vsplit(P, x) for val in sublist]   # 0度矩阵统计
    deg90 = [val.tolist() for sublist in np.split(np.transpose(P), y) for val in sublist]   # 90度矩阵统计
    diags = [P[::-1, :].diagonal(i) for i in range(-P.shape[0]+1, P.shape[1])]   #45度矩阵统计
    deg45 = [n.tolist() for n in diags]
    Pt = np.rot90(P, 3)   # 135度矩阵统计
    diags = [Pt[::-1, :].diagonal(i) for i in range(-Pt.shape[0]+1, Pt.shape[1])]
    deg135 = [n.tolist() for n in diags]

    def length(l):
        if hasattr(l, '__len__'):
            return np.size(l)
        else:
            i = 0
            for _ in l:
                i += 1
            return i

    glrlm = np.zeros((num_level, run_length, len(theta)))   # 按照统计矩阵记录所有的数据， 第三维度表示计算角度
    for angle in theta:
        for splitvec in range(0, len(eval(angle))):
            flattened = eval(angle)[splitvec]
            answer = []
            for key, iter in groupby(flattened):   # 计算单个矩阵的像素统计信息
                answer.append((key, length(iter)))
            for ansIndex in range(0, len(answer)):
                glrlm[int(answer[ansIndex][0]-min_pixels), int(answer[ansIndex][1]-1), theta.index(angle)] += 1   # 每次将统计像素值减去最小值就可以填入GLRLM矩阵中
    return glrlm


def apply_over_degree(function, x1, x2):
        rows, cols, nums = x1.shape
        result = np.ndarray((rows, cols, nums))
        for i in range(nums):
            print(x1[:, :, i])
            result[:, :, i] = function(x1[:, :, i], x2)
            print(result[:, :, i])
        result[result == np.inf] = 0
        result[np.isnan(result)] = 0
        return result


def calcuteIJ(rlmatrix):
        gray_level, run_length, _ = rlmatrix.shape
        I, J = np.ogrid[0:gray_level, 0:run_length]
        return I, J+1


def calcuteS(rlmatrix):
        return np.apply_over_axes(np.sum, rlmatrix, axes=(0, 1))[0, 0]


# --- Code below is works for `preproc.py` --- #
def get_all_files(path):
    # 读取所有文件路径
    file_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            file_list.append(os.path.join(dirpath, filename).replace('\\', '/'))
    return file_list


def split_train_test(wsi_list_path, train_num=40):
    for root, dirs, files in os.walk("/home1/gzy/Subtype/MetaData/2000"):
        folder_list = dirs
        if folder_list:
            break
    label_df = pd.read_csv(wsi_list_path)
    classes = label_df['label'].max() + 1
    train_set = pd.DataFrame()
    test_set = pd.DataFrame()
    for i in range(classes):
        train_index = []
        test_index = []
        temp_df = label_df[label_df['label'] == i]
        temp_df = temp_df.sample(frac=1)
        temp_df.reset_index(drop=True, inplace=True)
        for j in range(temp_df.shape[0]):
            if '.'.join(temp_df.loc[j, 'filename'].split('.')[:-1]) in folder_list and len(train_index) < train_num:
                train_index.append(j)
            else:
                test_index.append(j)
        if train_set.empty:
            train_set = temp_df.iloc[train_index]
        else:
            train_set = pd.concat([train_set, temp_df.iloc[train_index]], axis=0)
        if test_set.empty:
            test_set = temp_df.iloc[test_index]
        else:
            test_set = pd.concat([test_set, temp_df.iloc[test_index]], axis=0)
    train_set.to_csv("/home1/gzy/Subtype/train_labels.txt", index=False)
    test_set.to_csv("/home1/gzy/Subtype/test_labels.txt", index=False)


def get_mean_std(imgs_path):
    class CalMeanVar():
        def __init__(self):
            self.count = 0
            self.A = 0
            self.A_ = 0
            self.V = 0

        def cal(self, data):
            self.count += 1
            if self.count == 1:
                self.A_ = data
                self.A = data
                return
            self.A_ = self.A
            self.A = self.A + (data - self.A) / self.count
            self.V = (self.count - 1) / self.count ** 2 * (data - self.A_) ** 2 + (self.count - 1) / self.count * self.V

    img_h, img_w = 64, 64  # 根据自己数据集适当调整，影响不大
    num_C = 10000
    imgs_path_list = get_all_files(imgs_path)
    random.shuffle(imgs_path_list)
    B = CalMeanVar()
    G = CalMeanVar()
    R = CalMeanVar()
    for item in tqdm(imgs_path_list[:num_C]):
        img = cv.imread(item)
        img = cv.resize(img, (img_w, img_h))
        img = img / 255
        b_list = img[:, :, 0].ravel()
        g_list = img[:, :, 1].ravel()
        r_list = img[:, :, 2].ravel()
        for i in range(len(b_list)):
            B.cal(b_list[i])
            G.cal(g_list[i])
            R.cal(r_list[i])

    print("normMean = {}".format([R.A, G.A, B.A]))
    print("normStd = {}".format([np.sqrt(R.V), np.sqrt(G.V), np.sqrt(B.V)]))


if __name__ == '__main__':
    # wsi_list_path = "/home1/gzy/Subtype/labels.txt"
    # split_train_test(wsi_list_path, train_num=40)
    img_path = '/home1/gzy/NucleiSegmentation/CCRCC/NormImages'
    get_mean_std(img_path)
