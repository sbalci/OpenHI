import os
import math
import weakref
import json

import mmsi
import openhi
import openslide
import numpy as np
import cv2 as cv


SLIDE_READ_IMAGE = 1
SLIDE_READ_BOUND = 2
SLIDE_READ_ANNO = 4
SLIDE_READ_GRID = 8
SLIDE_READ_ALL = SLIDE_READ_IMAGE | SLIDE_READ_BOUND | SLIDE_READ_ANNO | SLIDE_READ_GRID

anno_color = [
    [0, 0, 0], [124, 252, 0], [0, 255, 255], [137, 43, 224],
    [255, 0, 255], [209, 104, 30], [255, 0, 0], [0, 255, 0]]
anno_alpha = 178


class _SlideMF:
    def __init__(self):
        manifest_path = openhi.root_path + '/framework_src/manifest.txt'
        wsis = open(manifest_path).readlines()
        self.manifest = [wsi.split('\t') for wsi in wsis]

    def get_id(self, line):
        return self.manifest[line][0]

    def get_filename(self, line):
        return self.manifest[line][1]

    def get_md5(self, line):
        return self.manifest[line][2]

    def get_size(self, line):
        return self.manifest[line][3]

    def get_state(self, line):
        return self.manifest[line][4]


class Slide:
    """A class representing a whole-slide image (WSI).

    Attributes
    ----------
    slide_id: int
        Slide ID according to :code:`/framework_src/manifest.txt`.
    file_path: str
        Full path to the slide file.
    available_pslvs: int[]
        The pre-segmentation levels (PSLV) allowed for this slide.
    bio_data: str
        Corresponding biospecimen data of the slide.
    clin_data: str
        Corresponding clinical data of the slide.

    """
    _mf = _SlideMF()

    def __init__(self, slide_index: int):
        """

        :param int slide_index: The line number of the slide in the manifest file.
        """
        # TODO: Look up the slide_id from the manifest file to establish full path.
        self.slide_id = self._mf.get_id(slide_index)
        self._file_dir = openhi.root_path + '/framework_src/data/' + self.slide_id + '/'
        self.file_path = self._file_dir + self._mf.get_filename(slide_index)
        self._file_name = self.file_path[:-4]

        # TODO: Check for the available PSLV and store the value here.
        self.available_pslvs = []
        self._pslv_paths = []
        self._pslv_images = []
        for pslv in eval(openhi.conf['annotation']['pslv_val']):
            pslv_path = self._file_name + '_super' + str(pslv) + "_bw.png"
            if os.path.isfile(pslv_path):
                self.available_pslvs.append(pslv)
                self._pslv_paths.append(pslv_path)
                self._pslv_images.append(cv.imread(pslv_path))

        self._oslide = openslide.open_slide(self.file_path)

        # TODO: Use the info module to extract the biospecimen and clinical data, then store them here.
        self.bio_data = None
        self.clin_data = None

        self._antrs = weakref.WeakValueDictionary()

    def __del__(self):
        self._oslide.close()

    def internal_crop_result(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, flags: int, pslv: int,
                             grid_density: int, img):
        """
        Internal usage only.
        """
        if viewing_size[0] <= 0 or viewing_size[1] <= 0 or viewer_size[0] <= 0 or viewer_size[1] <= 0:
            return np.zeros([1, 1, 3], np.uint8)

        img_size = self.get_size()
        if tl_coor[0] >= 0 and tl_coor[1] >= 0 and tl_coor[0] + viewing_size[0] <\
                img_size[0] and tl_coor[1] + viewing_size[1] < img_size[1]:
            ret_img = img(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density)
            if ret_img is None:
                ret_img = np.zeros([viewer_size[1], viewer_size[0], 3])
                ret_img.fill(255 if flags & SLIDE_READ_IMAGE else 0)
            return ret_img

        img_tl = [max(tl_coor[i], 0) for i in range(2)]
        img_sz = [min(tl_coor[i] + viewing_size[i], img_size[i]) - img_tl[i] for i in range(2)]
        dsr = [viewer_size[i] / viewing_size[i] for i in range(2)]
        rimg_sz = [int(img_sz[i] * dsr[i]) for i in range(2)]
        view_tl = [int(max(-tl_coor[i], 0) * dsr[i]) for i in range(2)]
        view_sz = [int(min(viewing_size[i], img_size[i] - tl_coor[i]) * dsr[i]) - view_tl[i] for i in range(2)]
        ret_img = img(tuple(img_tl), tuple(img_sz), tuple(rimg_sz), flags, pslv, grid_density)
        bg_img = np.zeros([viewer_size[1], viewer_size[0], 3])
        bg_img.fill(255 if flags & SLIDE_READ_IMAGE else 0)

        if ret_img is None:
            return bg_img
        bg_img[view_tl[1]:view_tl[1] + view_sz[1], view_tl[0]:view_tl[0] + view_sz[0]] = ret_img
        return bg_img

    def read_region(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, flags: int, pslv: int,
                    grid_density: int = 50, internal_check: bool = True) -> np.ndarray:
        """Read the original image, pslv boundary, grid or their combination.

        :param tl_coor: Coordination of top-left corner of the reading region.
        :param viewing_size: Size of the reading region.
        :param viewer_size: Required image size.
        :param flags: One or the combination of the followings:
            SLIDE_READ_IMAGE, SLIDE_READ_BOUND, SLIDE_READ_GRID
        :param pslv: Index of desired pslv.
        :param grid_density: Interval between grid lines (pixels).
        :param internal_check: Check the validity of parameters tl_coor, viewing_size and viewer_size.
            e.g. viewing_size exceeds image size, viewing_size is lower than 0, etc.
        :return: Result image in the same resolution with viewer_size.
        """
        if internal_check:
            return self.internal_crop_result(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density,
                                             self.internal_read_region)
        else:
            return self.internal_read_region(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density)

    def internal_read_region(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, flags: int, pslv: int,
                             grid_density: int):
        """
        Internal usage only.
        """
        img = None
        dsr = min(viewer_size[i] / viewing_size[i] for i in range(2))

        if flags & SLIDE_READ_IMAGE:
            img = self.read_image(tl_coor, viewing_size, viewer_size)
            img = cv.cvtColor(img, cv.COLOR_BGRA2RGB)

        if flags & SLIDE_READ_BOUND and dsr < 4:
            bound = self.read_boundary(tl_coor, viewing_size, viewer_size, pslv)
            cv.bitwise_not(bound, bound)
            if img is None:
                img = bound
            else:
                img *= (bound // 255)

        if flags & SLIDE_READ_GRID:
            grid = self.read_grid(viewing_size, viewer_size, grid_density)
            cv.bitwise_not(grid, grid)
            grid = cv.merge([grid, grid, grid])
            if img is None:
                img = grid
            else:
                img *= (grid // 255)

        return img

    def read_image(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple):
        """
        Internal usage only. Please use read_region with flags = SLIDE_READ_IMAGE instead.
        """
        dsr = min(viewer_size[i] / viewing_size[i] for i in range(2))
        level = self._oslide.get_best_level_for_downsample(dsr)
        reading_size = [int(viewing_size[i] / self._oslide.level_downsamples[level]) for i in range(2)]
        img = self._oslide.read_region(tl_coor, level, reading_size)
        return cv.resize(np.asarray(img), viewer_size)

    def read_boundary(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, pslv: int):
        """
        Internal usage only. Please use read_region with flags = SLIDE_READ_BOUND instead.
        """
        img = self._pslv_images[pslv][tl_coor[1]:tl_coor[1] + viewing_size[1], tl_coor[0]:tl_coor[0] + viewing_size[0]]
        return cv.resize(img, viewer_size)

    def read_grid(self, viewing_size: tuple, viewer_size: tuple, grid_density: int):
        """
        Internal usage only. Please use read_region with flags = SLIDE_READ_GRID instead.
        """
        mn_interval = float(openhi.conf['viewer_init']['smallest_grid_size'])
        pixel_size = self.get_pixel_size()[0]
        dsr = viewing_size[0] / viewer_size[0]
        grid_density *= pixel_size * dsr
        interval = mn_interval

        i, j = 1, 0
        while i < grid_density:
            if i > interval:
                interval = i
            i *= [2, 2.5, 2][j]
            j = (j + 1) % 3

        img = np.zeros([viewer_size[1], viewer_size[0]], dtype=np.uint8)
        cv.putText(img, 'GRID SIZE: %d um' % interval, (80, 80), cv.FONT_HERSHEY_SIMPLEX,
                   0.8, (255, 255, 255), 2, cv.LINE_8)

        interval = int(interval / dsr / pixel_size)
        for i in range(interval, viewer_size[0], interval):
            cv.line(img, (i, 0), (i, viewer_size[1]), 255)
        for i in range(interval, viewer_size[1], interval):
            cv.line(img, (0, i), (viewer_size[0], i), 255)
        return img

    def get_anno_mmsi_path(self, antr_id: int, pslv: int) -> str:
        """
        Get the mmsi file path to store the annotation.

        :param antr_id: Annotator id.
        :param pslv: Index of desired pslv.
        :return: Path of the mmsi file.
        """
        return self._file_name + "_antr" + str(antr_id) + "_super" + str(self.available_pslvs[pslv]) + ".mmsi"

    def get_anno(self, antr_id: int) -> 'SlideAnno':
        """Get the annotation object of specified annotator, which is used to read and write annotations.

        :param antr_id: Annotator id.
        :return: Annotation object.
        """
        antr = self._antrs.get(antr_id)
        if antr is None:
            antr = SlideAnno(self, antr_id)
            self._antrs[antr_id] = antr
        return antr

    def get_size(self) -> tuple:
        """Get the original slide size in pixels.

        :return: Slide resolution.
        """
        return self._oslide.dimensions

    def get_pixel_size(self) -> tuple:
        """Get the pixel size in microns.

        :return: Pixel size (x, y).
        """
        return (float(self.get_property(openslide.PROPERTY_NAME_MPP_X)),
                float(self.get_property(openslide.PROPERTY_NAME_MPP_Y)))

    def get_property(self, key: str) -> str:
        """Get the property value from OpenSlide.

        :param key: Property key.
        :return: Property value.
        """
        return self._oslide.properties[key]


class SlideAnno:
    """A class representing the annotation from one annotator of one slide. Please remember to delete
    the object after using to ensure annotation data is saved.

    Attributes
    ----------
    antr_id: int
        Internal annotator id.
    """
    UNDO_LIST_PROPERTY_NAME = "openhi_undo_list"

    def __init__(self, slide: Slide, antr_id: int):
        """
        Don't call this directly. Use Slide.get_anno instead.

        :param slide: The slide
        :param antr_id: Annotator id.
        """
        self._slide = slide
        self._annos = []
        self._undos = []
        for pslv in range(len(slide.available_pslvs)):
            mmsi_path = slide.get_anno_mmsi_path(antr_id, pslv)
            if os.path.isfile(mmsi_path):
                mmsi_obj, undo_obj = self._open_mmsi(mmsi_path)
                self._annos.append(mmsi_obj)
                self._undos.append(undo_obj)
            else:
                self._annos.append(None)
                self._undos.append(None)
        self.antr_id = antr_id

    def __del__(self):
        for i in range(len(self._slide.available_pslvs)):
            mmsi_obj, undo_obj = self._annos[i], self._undos[i]
            if mmsi_obj is not None:
                mmsi_obj.SetPropertyValue(SlideAnno.UNDO_LIST_PROPERTY_NAME, json.dumps(undo_obj))
                mmsi_obj.Close()

    @staticmethod
    def _open_mmsi(path: str):
        mmsi_obj = mmsi.Accessor(path)
        mmsi_obj.Open()
        mmsi_obj.SetSecureMode(True)
        mmsi_obj.SetCompressLevel(9)
        undo_str = mmsi_obj.GetPropertyValue(SlideAnno.UNDO_LIST_PROPERTY_NAME)
        undo_obj = [] if len(undo_str) == 0 else json.loads(undo_str)
        return mmsi_obj, undo_obj

    def read_region(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, flags: int, pslv: int,
                    grid_density: int = 50, internal_check: bool = True) -> np.ndarray:
        """Read the original image, pslv boundary, annotation result, grid or their combination.

        :param tl_coor: Coordination of top-left corner of the reading region.
        :param viewing_size: Size of the reading region.
        :param viewer_size: Required image size.
        :param flags: One or the combination of the followings:
            SLIDE_READ_IMAGE, SLIDE_READ_BOUND, SLIDE_READ_GRID, SLIDE_READ_ANNO
        :param pslv: Index of desired pslv.
        :param grid_density: Interval between grid lines (pixels).
        :param internal_check: Check the validity of parameters tl_coor, viewing_size and viewer_size.
            e.g. viewing_size exceeds image size, viewing_size is lower than 0, etc.
        :return: Result image in the same resolution with viewer_size.
        """
        if internal_check:
            return self._slide.internal_crop_result(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density,
                                                    self.internal_read_region)
        else:
            return self.internal_read_region(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density)

    def internal_read_region(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, flags: int, pslv: int,
                             grid_density: int):
        """
        Internal usage only.
        """
        img = self._slide.internal_read_region(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density)

        if flags & SLIDE_READ_ANNO:
            pslv = [x for x in range(len(self._slide.available_pslvs))]

            anno_layer = np.zeros([viewer_size[1], viewer_size[0], 3], dtype=np.uint8)
            for cur_pslv in pslv:
                anno = self.read_anno(tl_coor, viewing_size, viewer_size, cur_pslv)
                if anno is not None:
                    mask = cv.cvtColor(anno, cv.COLOR_RGB2GRAY).astype(np.uint8)
                    cv.threshold(mask, 1, 1, cv.THRESH_BINARY, mask)
                    for i in range(3):
                        anno_layer[:, :, i] = (anno[:, :, i] * mask + anno_layer[:, :, i] * (1 - mask))

            if img is None:
                img = anno_layer
            else:
                mask = cv.cvtColor(anno_layer, cv.COLOR_RGB2GRAY).astype(np.float)
                cv.threshold(mask, 1, anno_alpha / 255, cv.THRESH_BINARY, mask)
                for i in range(3):
                    img[:, :, i] = (anno_layer[:, :, i] * mask + img[:, :, i] * (1 - mask)).astype(np.uint8)

        return img

    def write_anno(self, points: [], pslv: int, grade: int, need_undo: bool = True):
        """Write annotation to the slide.

        :param points: An array of (x, y) coordinations of the annotation. e.g. [(100, 100), (200, 300)].
        :param pslv: Index of desired pslv.
        :param grade: Grade of the annotation.
        :param need_undo: Does this action need to undo.
        """
        bias = int(math.sqrt(self._slide.available_pslvs[pslv])) * 4
        posx = [point[0] for point in points]
        posy = [point[1] for point in points]
        mnx, mxx = max(0, min(posx) - bias), min(self._slide.get_size()[0], max(posx) + bias)
        mny, mxy = max(0, min(posy) - bias), min(self._slide.get_size()[1], max(posy) + bias)
        szx, szy = mxx - mnx, mxy - mny

        mask = np.zeros([szy + 2, szx + 2], dtype=np.uint8)
        bound = self._slide.read_boundary((mnx, mny), (szx, szy), (szx, szy), pslv)
        rmnx, rmxx, rmny, rmxy = szx, 0, szy, 0
        dis_points = []
        for point in points:
            rel = (point[0] - mnx, point[1] - mny)
            if bound[rel[1]][rel[0]][0] == 0 and mask[rel[1] + 1][rel[0] + 1] == 0:
                dis_points.append((int(point[0]), int(point[1])))
                _, _, _, rect = cv.floodFill(bound, mask, rel, 0, flags=8 | (grade << 8) | cv.FLOODFILL_MASK_ONLY)
                rmnx = min(rect[0], rmnx)
                rmxx = max(rect[0] + rect[2], rmxx)
                rmny = min(rect[1], rmny)
                rmxy = max(rect[1] + rect[3], rmxy)

        mask = mask[rmny + 1:rmxy + 1, rmnx + 1:rmxx + 1]
        mnx += rmnx
        mxx += rmxx - szx
        mny += rmny
        mxy += rmxy - szy

        if self._annos[pslv] is None:
            self._create_mmsi(pslv)
        mmsi_obj = self._annos[pslv]
        mmsi_obj.SetCurrentRegion((mnx, mny), (mxx - mnx, mxy - mny))

        orin_grades = []
        if need_undo:
            orin = mmsi_obj.ReadRegion(0)
            i = 0
            while i < len(dis_points):
                rel = (dis_points[i][0] - mnx, dis_points[i][1] - mny)
                orin_grade = int(orin[rel[1]][rel[0]])
                if orin_grade != grade:
                    orin_grades.append(orin_grade)
                    i += 1
                else:
                    dis_points.pop(i)
            if len(dis_points) > 0:
                self._undos[pslv].append((dis_points, orin_grades, int(grade)))

        mmsi_obj.WriteRegion(0, mask if grade > 0 else np.zeros([mxy - mny, mxx - mnx]), mask)

    def undo_anno(self, pslv: int, count: int = 1):
        """Undo the last modification(s) for specified pslv.

        :param pslv: Index of desired pslv.
        :param count: Number of action(s) to undo.
        """
        undo_obj = self._undos[pslv]
        if undo_obj is not None:
            count = min(count, len(undo_obj))
            for i in range(count):
                undo_item = undo_obj.pop()
                for j in range(len(undo_item[0])):
                    self.write_anno([undo_item[0][j]], pslv, undo_item[1][j], False)

    def read_anno(self, tl_coor: tuple, viewing_size: tuple, viewer_size: tuple, pslv: int):
        """
        Internal usage only. Please use read_region with flags = SLIDE_READ_ANNO instead.
        """
        if self._annos[pslv] is None:
            return None
        else:
            mmsi_obj = self._annos[pslv]
            mmsi_obj.SetCurrentRegion(tl_coor, viewing_size)
            dsr = min(viewer_size[i] / viewing_size[i] for i in range(2))

            level = 0
            for i in range(mmsi_obj.GetLevelCount()):
                if mmsi_obj.GetDownsample(i) < dsr:
                    level = i
                else:
                    break

            img = mmsi_obj.ReadRegion(level)
            img = cv.resize(img, viewer_size, interpolation=cv.INTER_NEAREST)
            rgb = [img, img.copy(), img.copy()]
            for i in range(len(anno_color)):
                for j in range(3):
                    rgb[j][rgb[j] == i] = anno_color[i][j]
            img = cv.merge(rgb)
            return img

    def _create_mmsi(self, pslv: int):
        if self._annos[pslv] is None:
            mmsi_path = self._slide.get_anno_mmsi_path(self.antr_id, pslv)
            mmsi_obj = mmsi.Writer(mmsi_path)
            mmsi_obj.SetImageSize(self._slide.get_size())
            mmsi_obj.SetTileSize((256, 256))
            mmsi_obj.SetPixelSize(1)
            mmsi_obj.Open()
            mmsi_obj.Close()
            self._annos[pslv], self._undos[pslv] = self._open_mmsi(mmsi_path)
