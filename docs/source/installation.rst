OpenHI Installation and Usage
=============================
To use OpenHI web interface, you need to install all necessary dependencies, OpenHI library, configure the framework to your need and run the web server. Only after that you can visit the framework by web browser of your choice.

Installation
------------
This installation guide includes all steps from installing required dependencies up to installing OpenHI.
There are some libraries used by OpenHI that requires additional installation (outside PyPI), including:

#. `OpenSlide <https://openslide.org>`_
#. `libMI and MMSI <https://gitlab.com/BioAI/libMI#how-to-build>`_
#. (For macOS and Windows) `Pyvips <https://libvips.github.io/libvips/install.html>`_

For Python dependencies, we will do that using :code:`pip`.

.. code-block:: bash

   pip install -r requirements.txt

After that, we build the MySQL table using script in the :code:`sql` folder of legacy the legacy module (:code:`module/legacy/mysql`). The current version is :code:`data_model_4-1`.

Lastly, install OpenHI library itself.

.. code-block:: bash

   python setup.py install

.. note:: If you are installing for development, OpenHI should be installed in development mode.

.. code-block:: bash

   python setup.py develop


Usage (running the framework)
------------------------------------

Before you can successfully run *OpenHI* you should configure the platform first. This consist of 3 steps: (1) configure the database (MySQL), (2) configure the OpenHI configuration file, and (3) run the main script (:code:`app.py`).

MySQL database configuration
#############################

**Database version**: v4.1

* Add the MySQL server configuration to the configuration file located in :code:`module/legacy/static/data_model_4-1_conf.ini`. Noted that this file can be based on the example file located at :code:`module/legacy/static/data_model_4-1_conf_example.ini`.
* Run the MySQL script to create the data schema in your MySQL server. Run: :code:`module/legacy/static/data_model_4-1.sql`
* Run the Python file to fill the newly created table with the clinical and biospecimen data (xml file) using python script. Run :code:`module/legacy/static/data_model_4-1_fill_info.py`.

OpenHI framework configuration
##############################

The OpenHI has one main external configuration file located at :code:`module/legacy/static/OpenHI_conf_example.ini` which includes the following configurations.

* MySQL setting

  * Host and port
  * Authentication (username and password)
  * OpenHI database schema name (include to support different data models)

* Web framework configuration

  * Host and port [default at 0.0.0.0 port 5000]
  * Running modes

* WSI

  * Default WSI for newly logged-in annotators.

.. TODO: Update configuration documentation.

Run the framework
###################

To run the framework run the main Flask python file which is `app.py` with the following command.

.. code-block:: bash

    cd module/legacy/
    python app.py

Try to visit the GUI using a web browser at the specified port. The default port is 5000, so try to visit :code:`http://127.0.0.1:5000`.

You should see the GUI like this.

.. image:: _static/gui_sample_1.png


Legacy installation
###################

To run OpenHIv1. You should follow the legacy installation guideline. OpenHIv1 release can be downloaded from the `Release Page <https://gitlab.com/BioAI/OpenHI/-/releases>`_

.. toctree::
    :maxdepth: 1

    installation-legacy
