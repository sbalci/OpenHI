# APIs
Actually this is documentation is more of an internal function explanation. 

Development file will start with `dev_*`, its purpose is to test out the a portion of the framework. 

## Web framework
- Main file: `anno_web.py`

### index()
The `index()` is a function to handle index page of the framework.
First image as a thumbnail is loaded to the template HTML file.
The thumbnail was created when the framework has started. 

### get_info()
This function is for the front-end to request for necessary information
to create the GUI corresponding to the WSI's property. 

### update_image()
For `input` and `output` of this function, 
please refer to `/documentation/front-back-end_interface.md`*

This function contains all the updating scripts to create new image
and return the new image URL to the front-end. 

### record()
The recording function parse a list of recording coordinates 
and save all of them to MySQL server. 

## Image processing
- Main file: `anno_img.py`

### get_assoc_coor(bwboun_img, top_left_coor, seedpoint)

This function will return a list of associated coordinates (x, y) correspond to
the given seedpoint and boundary image. 

- Input:
- `bwboun_img`:  (opencv - np array) black-white boundary image showing in the current viewer. 
This image can be called using `img.img_crop(...)`. 
- `top_left_coor`: (1x2 tuple) current top-left coordinate.
- `seedpoint`: (1x2 tuple) the seedpoint (point representing the sub-region) that we want to get
the associated coordinates. 

- return: a list of associated coordinates (x,y). 

### check_for_overlapping_regions(checking_img, bwboun_img, top_left_coor, seedpoints, grade)

This function will efficiently check the given list of seedpoint against existing
annotated regions. 

- input:
- `checking_img`: (opencv - np array) binary image of the annotated region. (This can be read from `static/`)
- `bwboun_img`:  (opencv - np array) black-white boundary image showing in the current viewer. This image can be called using `img.img_crop(...)`. 
- `top_left_coor`: (1x2 tuple) current top-left coordinate.
- `seedpoints`: (nx2 tuple) a list of seedpoints that we want to check if overlapping sub-regions for each of them exist. There could be n points to be checked. 
- `seedpoints`: (int) grade that is needed to be checked.

- return: (nx2 tuple) the status of each seedpoints. If the seedpoint overlaps with existing annotated region(s), the corresponding location of the list will return 1, and it will return 0 otherwise. The 1st column will be return the result correspond to the global check, and the 2nd column will check against the specified grading. 

### add_grid(img, size_viewing, size_viewer, pixel_size=0.25)

This function overlay grid on to the input image. The grid size is made based on the given viewing area value. 
The pixel size (physical size of the glass slide that was captured by one digital pixel) is set to default of 0.25 
micron/pixel which is based on 40x scans. The size of the grid is specified as embedded text on the top-left of 
the output image. 

#### parameters

- input:
- `img` (opencv - np array) Full BRG color viewer image for overlaying the grid on. 
- `size_viewing` (1x2 list) OpenHI viewing area size.
- `size_viewer` (1x2 list) OpenHI viewer size. 
- `pixel_size` (float) (optional: default = 0.25) Physical size of one pixel in micron/pixel. (Typically 0.25 for 40x scan, and 0.5 for 20x scan) Noted that this value can be extracted from the WSI's metadata. 

- return: (opencv - np array) Input image with the overlaying grid if the number of grid does not exceed 10 lines
while the maximum grid size is 50 um. 

#### internal adjustment
- The color of the overlaying grid can be adjust at the Generate color (RGB) grid section by adjusting the color value of each BRG layer individually. The possible value is 0 to 255. 
- The transparency of the overlaying grid can be adjust in the Alpha blend with masking section. The possible value is 0 to 1. 
- The position of the embedded text can be adjusted at the Grid size text embedding section. 

## MySQL
**Main file:** `anno_sql.py`

### get_record(db, tl, sz, pslv, annotator_id, slide_id)
Get the annotation record from MySQL server based on specified area, annotator id, and slide id. 
- db: MySQL database object of the framework
- tl: top-left coordinate
- sz: viewing area size
- pslv: requested pre-defined segmentation level
- annotator_id: requested annotator id
- slide_id: requested slide id

- return: A list of annotation point in the specified area at speficied 
presegmentation level. Noted that these points are absolute coordinate. 
The structure of the returned list should consist of 3 lists containing: 
  - x coordinate
  - y coordinate
  - grading of the point

### create_connector() 
Connect to MySQL server and database based on preset values. Return database object.

- require: OpenHI configuration file. Example _conf_ can be found at `static/OpenHI_conf.ini`. 

- return: OpenHI MySQL database object


## Objects
In the annotation software, there are many class-object which are concepts
that the developers need to understand before proceed with the extension
of this software. 

### Loader
Syntax: `Loader()`

The loader is an object that contains the whole current WSI and its
associated information such as superpixel boundaries. It is also a place to
keep the record of which annotator is looking at the image and where the 
annotator is currently looking at. 
