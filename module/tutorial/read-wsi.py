"""Simple tutorial  on how to create WSI object using OpenHI Slide class.

"""
from openhi.img.slide import Slide
from openhi.img.dev_img_toolbox import imshow_sk

if __name__ == '__main__':
    s1_id = 1  # Set slide ID

    # Create a new slide object
    slide_1 = Slide(s1_id)

    # Let's view the top-left 10x10 portion of the loaded WSI.

    # Check the image size using `get_size()` method, so we know what can be loaded.
    s1_size = slide_1.get_size()
    print(s1_size)

    # Config the slide loading parameters
    tl_coor = (6000, 3000)
    viewing_size = (400, 400)
    viewer_size = (400, 400)
    flags = 1
    pslv = 0
    grid_density = 10
    internal_check = True

    # Load part of the slide
    part_of_slide = slide_1.read_region(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density, True)
    imshow_sk(part_of_slide, 1)  # Show the image.

    # That's a little too close! Let's zoom out.
    part_of_slide = slide_1.read_region((5000, 1000), (2000, 2000), (600, 600), 1, 0, 10, True)
    imshow_sk(part_of_slide, 1)  # Show the image.


