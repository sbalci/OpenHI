# Data downloading module

## How to download data
(Information here is still inaccurate)
1. The image data of TCGA is in [GDC database](https://portal.gdc.cancer.gov/repository)
2. By visiting the website, we could download data in form of tabulation. The tabulation contains some information, including the id.
3. Finding out the id, producing the link: [https://api.gdc.cancer.gov/data/+id](https://api.gdc.cancer.gov/data/+id)
4. Download the needed data according to the link.
