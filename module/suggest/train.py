import PIL
import random

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from module.suggest.models import VGG, GoogLeNet, MobileNet


def train_network(data_dir: str, save_fname: str, classes: int = 3, epoch: int = 100, batch_size: int = 128,
                  learn_rate: float = 0.001, momentum: float = 0.9, use_gpu: bool = True, gpu_id:int = 0):
    """Train CNN model using given dataset.

    :param data_dir: Directory of the dataset.
    :param save_fname: File path to save the trained model.
    :param classes: Number of cancer grades.
    :param epoch: Training epoch.
    :param batch_size: Training batch size.
    :param learn_rate: Learning rate for SGD.
    :param momentum: Momentum for SGD.
    :param use_gpu: Use GPU to train or not.
    :param gpu_id: Device id of GPU.
    :return: Best loss and accuracy of the model.
    """
    if use_gpu:
        torch.cuda.set_device(gpu_id)
    preprocess = transforms.Compose([
        transforms.ToTensor()
    ])

    def default_loader(path):
        return preprocess(PIL.Image.open(path))

    train_labels = []
    with open(data_dir + '/train_labels.txt') as input_file:
        train_labels = input_file.readlines()
    random.shuffle(train_labels)

    class TrainSet(Dataset):
        def __init__(self, loader=default_loader):
            self.lines = train_labels
            self.loader = loader

        def __len__(self):
            return len(self.lines)

        def __getitem__(self, item):
            img_name, label = self.lines[item].split(' ')
            img = self.loader(data_dir + '/' + img_name)
            return img, int(label) - 1

    train_loader = DataLoader(TrainSet(), batch_size=batch_size, shuffle=True)

    model = VGG(classes)
    if use_gpu:
        model = model.cuda()

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=learn_rate, momentum=momentum)

    min_loss = 1
    max_acc = 0.0

    for e in range(1, epoch + 1):
        model.train()
        train_loss = 0
        correct = 0
        for batch_idx, (data, target) in enumerate(train_loader):
            data, target = Variable(data), Variable(target)
            if use_gpu:
                data = data.cuda()
                target = target.cuda()
            optimizer.zero_grad()
            output = model(data)
            loss = criterion(output, target)
            train_loss += loss.data
            loss.backward()
            optimizer.step()

            pred = output.data.max(1, keepdim=True)[1]
            correct += pred.view_as(target.data).eq(target.data).cpu().sum()
            if batch_idx % 10 == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    e, batch_idx * len(data), len(train_loader.dataset),
                    100. * batch_idx / len(train_loader), loss.data))

        aver_loss = train_loss / len(train_loader.dataset)
        accuracy = int(correct) / len(train_loader.dataset)
        if aver_loss < min_loss:
            torch.save(model.state_dict(), save_fname)
            min_loss = aver_loss
            max_acc = accuracy

    return float(min_loss), float(max_acc)
