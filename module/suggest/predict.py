import PIL
import random

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from module.suggest.models import VGG, GoogLeNet, MobileNet


def predict_grade(data_dir: str, model_fname: str, save_fname: str, classes: int = 3, batch_size: int = 128,
                  use_gpu: bool = True, gpu_id: int = 0):
    """Use trained model to predict cancer grade.

    :param data_dir: Directory of the dataset.
    :param model_fname: File path of the trained model.
    :param save_fname: File path to save the prediction result.
    :param classes: Number of cancer grades.
    :param batch_size: Testing batch size.
    :param use_gpu: Use GPU to predict or not.
    :param gpu_id: Device id of GPU.
    """
    if use_gpu:
        torch.cuda.set_device(gpu_id)
    preprocess = transforms.Compose([
        transforms.ToTensor()
    ])

    def default_loader(path):
        return preprocess(PIL.Image.open(path))

    anno_labels = []
    with open(data_dir + '/anno_labels.txt') as input_file:
        anno_labels = input_file.readlines()

    class AnnoSet(Dataset):
        def __init__(self, loader=default_loader):
            self.lines = anno_labels
            self.loader = loader

        def __len__(self):
            return len(self.lines)

        def __getitem__(self, item):
            img_name = self.lines[item].split('\n')[0]
            return img_name, self.loader(img_name)

    anno_loader = DataLoader(AnnoSet(), batch_size=batch_size, shuffle=False)

    model = VGG(classes)
    if use_gpu:
        model = model.cuda()

    model.load_state_dict(torch.load(model_fname))
    model.eval()
    res = open(save_fname, 'w')
    for file, data in anno_loader:
        data = Variable(data)
        if use_gpu:
            data = data.cuda()
        output = model(data)
        pred = output.data.max(1, keepdim=True)[1]
        for i in range(0, len(pred)):
            res.write('{} {}\n'.format(file[i], int(pred[i])))
