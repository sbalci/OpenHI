# Web framework for whole-slide image annotation.
#   Created on 20180319
import os
import json
import math
import configparser
import logging

from flask import Flask, render_template, jsonify, request, flash, redirect, url_for, session, g
from module.legacy.forms import LoginForm

from openhi.legacy import anno_img as img, anno_web as web, anno_sql as sql
from openhi.legacy.anno_web import Clr

#subprocess.Popen(['script','-a','log.txt'],stdout = an_open_writeable_file_object)

# Initialization of the framework
app = Flask(__name__)   # Initialize Flask
app.config['SECRET_KEY'] = '14a194b474e31af27daf2bc52f3a78bf'

# log configuration
fmt = '%(asctime)s - %(levelname)s - %(filename)s - %(message)s'
logging.basicConfig(level=logging.DEBUG,format=fmt)
logger = logging.getLogger('app')
handler=logging.FileHandler(filename='app.log')
handler.setLevel(logging.DEBUG)
formatter=logging.Formatter(fmt)
handler.setFormatter(formatter)
logging.getLogger('').addHandler(handler)

# Read framework configuration from INI configuration file.
# --Check if the local configuration file exists, if not run based on the example file.
file_checking = 'static/OpenHI_conf.ini'
checking_status = os.path.isfile(file_checking)
if not checking_status:
    open_file_name = 'static/OpenHI_conf_example.ini'
else:
    open_file_name = file_checking

# --Read the OpenHI INI configuration file.
conf = configparser.ConfigParser()
conf.read(open_file_name)

# Check for cache directory
dir_checking = 'static/cache'
checking_status = os.path.exists(dir_checking)

print('Did the checking directory exists?: ' + str(checking_status))    # Print the checking result
if not checking_status:
    os.mkdir(dir_checking)
    print(Clr.BOLD + 'The directory "' + dir_checking + '" did not exists and it has been created.' + Clr.end)
else:
    print(Clr.BOLD + 'The directory "' + dir_checking + '" already exists. No further actions are required.' + Clr.end)

loader = img.LoaderConf()
rand_url = web.WebInter()               # rand_url stands for random URL
db = sql.create_connector()             # db stands for database
s_obj = sql.CurrentStaticObject()       # s_obj stands for static object (not changing during every clicks)
pt = sql.PointDynamicObject()           # pt stands for point
pt_sucess = sql.PointDynamicObject()    # pt_success stands for point pass check before record
pt_false = sql.PointDynamicObject()     # pt_false stands for point fail to pass check before record
asess = web.AnnotatorSessionList()      # asess stands for annotator session

# Initialize 3 annotators
init_vp = img.ViewingPosition()     # vp is 'viewing position'
manifest_line_number = int(conf['viewer_init']['slide_id']) - 1
init_vp.load_from_config(conf['viewer_init']['viewer_coor'])
asess.init_new_annotator(1, int(conf['viewer_init']['slide_id']), init_vp, sql.re_create_connector(db))
asess.init_new_annotator(2, int(conf['viewer_init']['slide_id']), init_vp, sql.re_create_connector(db))
asess.init_new_annotator(3, int(conf['viewer_init']['slide_id']), init_vp, sql.re_create_connector(db))

# Ignore human pixel size while calculating virtual magnification
ignore_hps = conf['virtual_mag']['ignore_human_pixel_size'] == 'True'

# Load the first WSIs (no overlaying layer)
img.call_wsi(asess.get_loader_obj(1), init_vp.coor_tl, init_vp.size_viewing, init_vp.size_viewer)
img.wsi_get_thumbnail(asess.get_loader_obj(1), rand_url.current, init_vp.size_viewer)
print('first image is saved as: ' + rand_url.current)

@app.before_request
def before_request():
    try:
        g.user = None
        if 'user' in session:
            g.user = session['user']
    except Exception as e:
        logging.exception('Got exception on before_request handler')
        raise


@app.route('/', methods=['GET', 'POST'])
def index():
    try:
        if g.user:
            page_anno_id = session['user']
            option_ctr_r_appear = False

            if option_ctr_r_appear:
                output_slide = '/static/images/OHI_error_image.png'
            else:
                # URL to the blank image
                output_slide = '/static/images/BlankImage.png'
            return render_template(
                'mytemplate_with_comments.1.html',
                slide_url=output_slide,
                anno_id=page_anno_id
            )

        return redirect(url_for('login'))
    except Exception as e:
        logging.exception('Got exception on index handler')
        raise

@app.route('/login/', methods=['POST', 'GET'])
def login():
    try:
        form = LoginForm()
        if form.validate_on_submit():
            session.pop('user',None)
            username = form.username.data
            password = form.password.data

            login_sql = 'SELECT annotator_id,password from annotator WHERE annotator_id=\''+username+"'"
            db1=sql.re_create_connector(db)
            mycursor = db1.cursor()
            mycursor.execute(login_sql)
            tmp= mycursor.fetchone()
            mycursor.close()

            if password==tmp[1]:
                session['user']=username

                # Get current viewing slide of the specific user
                anno_id = username
                slide_id = asess.get_slide_id(anno_id)
                session['slide_id'] = slide_id

                # Initialize the annotator's WSI
                img.call_wsi(asess.get_loader_obj(int(anno_id)), init_vp.coor_tl, init_vp.size_viewing, init_vp.size_viewer)
                img.wsi_get_thumbnail(asess.get_loader_obj(int(anno_id)), rand_url.current, init_vp.size_viewer)
                print('first image is saved as: ' + rand_url.current)

                return redirect(url_for('index'))
            else:
                flash("Password is wrong.Please try again.")
                return render_template('login.html', form=form)

        return render_template("login.html", form=form)
    except Exception as e:
        logging.exception('Got exception on login handler')
        raise

@app.route('/_get_info')  #get the information of current image
def get_info():
    try:
        anno_id = session['user']
        local_loader = asess.get_loader_obj(anno_id)

        w = local_loader.imgWSIWidth
        h = local_loader.imgWSIHeight
        sending_pslv_number = local_loader.superpixelLv - 1   # In front-end, pslv starts from 0
        print('Returning image size')
        print('Image size is: ' + str(w) + ' by ' + str(h))
        print('Sending JSON')

        return jsonify(img_width=w, img_height=h, ps_lv=sending_pslv_number)
    except Exception as e:
        logging.exception('Got exception on get_info handler')


@app.route('/_get_WSI_meta')
def get_wsi_meta():
    try:
        # ----- [ Getting slide properties ] ----- #
        anno_id = session['user']
        local_loader = asess.get_loader_obj(anno_id)
        # Print all available properties
        list_of_properties = list(local_loader.ptr.properties)  # get the list
        print('\n This is the start of the list: ')
        for line in list_of_properties:
            print(line)

        return jsonify(magnification=local_loader.ptr.properties['aperio.AppMag'],
                       comment=local_loader.ptr.properties['openslide.comment'],
                       height_level0=local_loader.ptr.properties['openslide.level[0].height'],
                       width_level0=local_loader.ptr.properties['openslide.level[0].width'],
                       objective_power=local_loader.ptr.properties['openslide.objective-power'],
                       vendor=local_loader.ptr.properties['openslide.vendor'])

        # Example on how to get the value of the property
        #   Property from the vendor (aperio.*)
        print('The magnification of the WSI is: ')
        print(local_loader.ptr.properties['aperio.AppMag'])

        #   Standard property from OpenSlide (openslide.*)
        # (full reference: https://openslide.org/api/python/#standard-properties)
        print('Comment that OpenSlide can read from: ')
        print(local_loader.ptr.properties['openslide.comment'])

        print('Original WSI height (level 0): ')
        print(local_loader.ptr.properties['openslide.level[0].height'])

        print('Original WSI downsample(level 0):')
        print(local_loader.ptr.properties['openslide.level[0].downsample'])
    except Exception as e:
        logging.exception('Got exception on get_wsi_meta handler')
        raise


@app.route('/get_patient_meta/', methods=['POST'])
def get_patient_meta():
    try:
        # Get slide ID from user cookie-session
        slide_id = session['slide_id']
        json_type = request.get_data()
        print(json_type)
        string_type = json.loads(json_type)
        db1 = sql.re_create_connector(db)
        result = sql.get_patient_data(string_type, str(slide_id), db1)

        result = json.dumps(result)

        return result
    except Exception as e:
        logging.exception('Got exception on get_patient_meta handler')
        raise


@app.route('/get_bio_meta/', methods=['POST'])
def get_bio_meta():
    try:
        # Get slide ID from user cookie-session
        slide_id = session['slide_id']

        json_type = request.get_data()
        string_type = json.loads(json_type)
        db1 = sql.re_create_connector(db)
        result = sql.get_bio_data(string_type, str(slide_id), db1, True)
        result = json.dumps(result)

        return result
    except Exception as e:
        logging.exception('Got exception on get_bio_meta handler')
        raise


@app.route('/_update_image')
def update_image():
    try:
        # Request for annotator id.
        anno_id = session['user']
        slide_id = session['slide_id']

        # Get local loader
        local_loader = asess.get_loader_obj(anno_id)

        error_message = 'N/A'
        request_status = 0

        v1 = request.args.get('var1', 0, type=int)
        v2 = request.args.get('var2', 0, type=int)
        v3 = request.args.get('var3', 0, type=int)
        v4 = request.args.get('var4', 0, type=int)
        v5 = request.args.get('var5', 0, type=int)
        v6 = request.args.get('var6', 0, type=int)
        v7 = request.args.get('var7', 0, type=int)
        v8 = request.args.get('var8')

        # Acquire 'var8' and parse the string into a tuple.
        invalid_points = eval(v8)

        # Update current pslv in the loader. (single-user)
        local_loader.current_pslv = v7  # v7=0

        img_size_w = local_loader.imgWSIWidth
        img_size_h = local_loader.imgWSIHeight

        loc_tl_coor = (v1, v2)
        loc_viewing_size = (v3 - v1, v4 - v2)
        loc_viewer_size = (v5, v6)

        local_loader.update_viewersize(loc_viewing_size)

        # Update URL configuration
        last_url = rand_url.current   # generate a random name of an image
        rand_url.get_new_url()
        slide_url = rand_url.current

        # Set maximum zoom-out to view annotation and boundary.
        no_boundary_condition = False
        max_viewing = max(loc_viewing_size)
        max_support_viewing = 3000
        # max_support_viewing: Adjustable: to disable annotation and boundary generation that slows the framework down.
        if max_viewing > max_support_viewing:
            no_boundary_condition = True

        # Check for thumbnail condition
        if (loc_tl_coor[0] == 0) and(loc_tl_coor[1] == 0):
            print('Loading image top-level thumbnail')
            # store the image to the random jpg file
            img.wsi_get_thumbnail(local_loader, rand_url.current, loc_viewer_size)
            slide_url = rand_url.current
            web.rm_file(last_url)
            # request_status = 'Zooming is too far away, not showing the boundary and annotation.'

        elif no_boundary_condition:
            print('Load without annotation (too far away to generate boundary)')
            saving_status = img.call_wsi(local_loader, loc_tl_coor, loc_viewing_size, loc_viewer_size,
                                         write_url=slide_url)
            web.rm_file(last_url)

        # Catch error when front-end request image smaller than 1 by 1 pixel. (should not happen anymore)
        elif (loc_viewing_size[0] < 1) or (loc_viewing_size[1] < 1):
            request_status = 'Front-end error'
            error_message = '@back-end: The viewing size is less than 1 (Related to input var1-4)'
            slide_url = last_url    # Return last known image without deleting the image.

        # Normal update image routine
        else:
            try:
                print("!try")
                db1 = sql.re_create_connector(db)
                # Generate new image and save at specified URL. If the script fail, it would probably fail here.
                img.gen_image_with_annotation(local_loader, loc_tl_coor, loc_viewing_size, loc_viewer_size, slide_url,
                                              db1, anno_id, slide_id, asess, invalid_points)
                web.rm_file(last_url)

            except Exception as e:
                print("!except")
                db1 = sql.re_create_connector(db)
                img.gen_image_with_annotation(local_loader, loc_tl_coor, loc_viewing_size, loc_viewer_size, slide_url,
                                              db1, anno_id, slide_id, asess, invalid_points)
                web.rm_file(last_url)
                rand_url.current = slide_url = last_url
                request_status = 'Python catch condition'
                error_message = '@back-end: Did not successfully update URL, update image.' \
                                + '| System message: ' + ' ... ' + str(e)

        print(Clr.BOLD + 'Sending JSON' + Clr.END)
        print(Clr.BOLD + 'Refreshing information: ' + Clr.END)
        print('  Annotator ID: ' + str(anno_id))
        print('  Slide ID: ' + str(slide_id))
        return jsonify(
            slide_url=slide_url,
            img_size_width=img_size_w,
            img_size_height=img_size_h,
            top_left_x=loc_tl_coor[0],
            top_left_y=loc_tl_coor[1],
            viewing_size_x=loc_viewing_size[0],
            viewing_size_y=loc_viewing_size[1],
            exit_code=request_status,
            error_message=error_message
    )
    except Exception as e:
        logging.exception('Got exception on update_image handler')
        raise


@app.route('/_record', methods=['GET', 'POST'])
def record():
    try:
        anno_id = session['user']
        slide_id = session['slide_id']

        local_loader = asess.get_loader_obj(anno_id)

        # req_data = request.get_json()
        req_form = request.form

        num_of_points = int(len(req_form)/4)

        # Access info
        pt_list_att = ('[x]', '[y]', '[grading]', '[ps_lv]')

        # In one round, the grading and pslv could be updated only once.
        pt.update_grading(req_form[str(0) + pt_list_att[2]])
        pt.update_pslv(req_form[str(0) + pt_list_att[3]])
        print('-----------record in app.py, current pt.pslv = %s' % pt.pslv)

        pt.update_annobatch(local_loader.maxAnnobatch)
        print('--------------------maxAnnobatch: %s-------------------' % local_loader.maxAnnobatch)
        # IF: detect grading == 0
        # (change to deleting routine)
        # (...)

        # Else: (record as normal)

        for pt_id in range(num_of_points):
            pt.add_point_dump(req_form[str(pt_id) + pt_list_att[0]], req_form[str(pt_id) + pt_list_att[1]])

            # if pt_id == (num_of_points-1):
            #   print(str(pt_id+1) + ' points has been generated for recording')

        # If grading == 0 then it's deletion
        print('Current grading before recording: ')
        print(pt.grading)
        if (pt.grading == 0) or (pt.grading == '0'):
            print('Deleting ...')
            db1=sql.re_create_connector(db)
            num_delete = sql.delete_point(db1, pt, local_loader, anno_id, slide_id)
            print('%s points has been generated for recording' % num_delete)
            status = 'delete successful'

        else:
            # Insert the recording point validation code in this section ###
            pt_sucess.clear_point_dump()
            pt_false.clear_point_dump()
            pt_sucess.update_grading(req_form[str(0) + pt_list_att[2]])
            pt_sucess.update_pslv(req_form[str(0) + pt_list_att[3]])
            pt_sucess.update_annobatch(local_loader.maxAnnobatch)
            pt_false.update_grading(req_form[str(0) + pt_list_att[2]])
            pt_false.update_pslv(req_form[str(0) + pt_list_att[3]])
            pt_false.update_annobatch(local_loader.maxAnnobatch)
            checking_img = asess.get_current_annotation_bin(anno_id)
            bwboun_img = img.img_crop(local_loader.imgBWBoun[local_loader.current_pslv], local_loader.imgTLCorr,
                                      local_loader.viewersize, 0)
            # seedpoints = [[1800, 1800], [2200, 2200]]
            for i in range(len(pt.x)):
                seedpoints = [[int(float(pt.x[i])),int(float(pt.y[i]))]]
                returning_result = img.check_for_overlapping_regions_debug(checking_img, bwboun_img, local_loader.imgTLCorr,
                                                                           seedpoints)
                print('The checking result is: ')
                print(returning_result[0,0])
                if not returning_result[0,0]:
                    pt_sucess.add_point_dump(pt.x[i], pt.y[i])
                else:
                    pt_false.add_point_dump(pt.x[i], pt.y[i])
        # --------------------------[end]--------------------------- ###

            # Send record to MySQL database
            print('Recording ...')
            db1=sql.re_create_connector(db)
            num_record = sql.record_point2(db1, local_loader.imgTLCorr, local_loader.viewersize, anno_id, slide_id,
                                           pt_sucess, local_loader, asess)
            print('%s points has been generated for recording' % num_record)
            status = 'record successful'

        pt.clear_point_dump()

        print(status)
        return jsonify(status=status,pt_false_x =pt_false.x, pt_false_y=pt_false.y,pt_sucess_x =pt_sucess.x, pt_sucess_y=pt_sucess.y)
    except Exception as e:
        logging.exception('Got exception on record handler')
        raise


@app.route('/_undo')
def undo():
    try:
        local_loader = asess.get_loader_obj(session['user'])
        cmd = request.args.get('cmd', 0, type=int)
        print('Received command: ' + str(cmd))

        # Get annotator and slide ID from cookie-session
        anno_id = session['user']
        slide_id = session['slide_id']
        db1=sql.re_create_connector(db)
        sql.undo_batch(db1, cmd, anno_id, slide_id, local_loader)

        print('undo successfully')
        return jsonify(status='successful')
    except Exception as e:
        logging.exception('Got exception on undo handler')
        raise

@app.route('/_annotator_id')
def annotator_id():
    try:
        cmd = request.args.get('id', 0, type=int)
        print('Received command: ' + str(cmd))

        # sql.undo_batch(db, cmd)
        # newAnnobatch = sql.get_batch(db)
        # loader.update_annobatch(newAnnobatch)

        # print('undo successfully')
        return jsonify(status='successful', result=session['user'])
    except Exception as e:
        logging.exception('Got exception on annotator_id handler')
        raise


# Function for image sub-region boundary switch
@app.route('/_tog_boun')
def tog_boun():
    try:
        local_loader = asess.get_loader_obj(session['user'])
        local_loader.tog_boun()

        return jsonify(status='toggled', result=local_loader.togBounSwitch)
    except Exception as e:
        logging.exception('Got exception on tog_boun handler')
        raise


@app.route('/logout/', methods=['POST', 'GET'])
def logout():
    try:
        session.pop('user', None)
        flash("You have logged out.")
        return redirect(url_for('index'))
    except Exception as e:
        logging.exception('Got exception on logout handler')
        raise

# -- To be removed because of problematic OSD icon loading error.
# @app.context_processor  # empty the browser cache
# def override_url_for():
#     return dict(url_for=dated_url_for)
#
#
# def dated_url_for(endpoint, **values):
#     if endpoint == 'static':
#         filename = values.get('filename', None)
#         if filename:
#             file_path = os.path.join(app.root_path,
#                                      endpoint, filename)
#             values['q'] = int(os.stat(file_path).st_mtime)
#     return url_for(endpoint, **values)


@app.route('/_calc_mag')
def calc_mag():
    try:
        local_loader = asess.get_loader_obj(session['user'])
        width = request.args.get('swidth', 0, type = int)
        height = request.args.get('sheight', 0, type = int)
        screen_res = (width, height)

        size = request.args.get('ssize', 0, type = float)
        screen_size = size

        distance = request.args.get('sdis', 0, type = float)
        screen_dis = distance

        whratio = screen_size * screen_size / (screen_res[0] * screen_res[0] + screen_res[1] * screen_res[1])
        screen_ps = math.sqrt(whratio) * 2.54   # Note: ps = pixel size

        min_angle = 1.22 * 0.55 / 3000
        human_ps = min_angle * screen_dis

        image_ps = float(local_loader.get_mpp()[0])

        show_w = request.args.get('brx', 0, type = float) - request.args.get('tlx', 0, type = float)
        show_h = request.args.get('bry', 0, type = float) - request.args.get('tly', 0, type = float)

        img_w = request.args.get('iwidth', 0, type = int)
        img_h = request.args.get('iheight', 0, type = int)

        res = (screen_ps * 10000 / image_ps) * (img_w / show_w)
        if not ignore_hps:
            res = res * (screen_ps / human_ps)

        return jsonify(status = 'successful', mag = ('%.2f' % res), abs = res)
    except Exception as e:
        logging.exception('Got exception on calc_mag handler')
        raise


@app.route('/_change_slide_id')
def _change_slide_id():
    anno_id = int(session['user'])
    old_slide_id = session['slide_id']

    # Get the new slide id
    new_slide_id = request.args.get('id', 0, type=int)

    try:
        # Update new slide id into cookie-session
        session['slide_id'] = new_slide_id
        db1 = sql.re_create_connector(db)

        # Use annotator object to initialize new annotator object with specified slide id.
        asess.set_slide_id(anno_id, new_slide_id, db1)

        # report
        print('Slide ID update report: ')
        print('old slide ID: ' + str(old_slide_id))
        print('new slide ID: ' + str(new_slide_id))

        # Re initialize the WSI
        local_loader = asess.get_loader_obj(anno_id)

        w = local_loader.imgWSIWidth
        h = local_loader.imgWSIHeight

        # Update URL configuration
        last_url = rand_url.current
        rand_url.get_new_url()
        slide_url = rand_url.current

        img.gen_image_with_annotation(local_loader, (0, 0), (100, 100), (100, 100), slide_url, db1,
                                      anno_id, new_slide_id, asess, ())

        web.rm_file(last_url)
        rand_url.current = last_url

        img.call_wsi(asess.get_loader_obj(anno_id), init_vp.coor_tl, init_vp.size_viewing, init_vp.size_viewer)
        img.wsi_get_thumbnail(asess.get_loader_obj(anno_id), rand_url.current, init_vp.size_viewer)

        print('first image is saved as: ' + rand_url.current)

        return jsonify(status='slide id update successful', result=new_slide_id)
    except AttributeError as e:
        print('Update slide error message: ')
        print(e)

        print(Clr.BOLD + 'End of error message' + Clr.END)
        return jsonify(status='Slide ID can only be a number.')
    except Exception as e:
        logging.exception('Got exception on change_slide_id handler')
        raise

if __name__ == '__main__':
    try:
        # Read necessary information from the configuration dictionary object
        framework_host = conf['framework']['host']
        framework_port = int(conf['framework']['port'])
        framework_debug = conf['framework']['debug'] == 'True'
        framework_reloader = conf['framework']['reloader'] == 'True'

        app.run(
            host=framework_host,
            debug=framework_debug,      # debug=True is running in debug mode
            port=framework_port,
            use_reloader=framework_reloader,
            threaded=True   # Currently not in the configuration INI file.
        )
    except Exception as e:
        logging.exception('Got exception on main handler')
