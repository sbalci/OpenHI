USE `db_wsi4_dev`;

ALTER TABLE annotator DROP annotator_name;
ALTER TABLE biospecimen DROP FOREIGN KEY tcga_case_id_in_bio;
ALTER TABLE biospecimen DROP tcga_case_id;
