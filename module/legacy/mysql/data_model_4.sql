-- Create the schema version 4

CREATE DATABASE `db_wsi4_dev` ; -- OR `db_wsi1`

USE `db_wsi4_dev`;

CREATE TABLE `annotator` (
  `annotator_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NA',
  PRIMARY KEY (`annotator_id`)
) ENGINE=InnoDB ;


CREATE TABLE `patient` (
  `patient_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tcga_case_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `patient_info` JSON,
  PRIMARY KEY (`patient_id`),
  INDEX(tcga_case_id)
) ENGINE=InnoDB ;

CREATE TABLE `biospecimen` (
  `bio_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tcga_case_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `bio_info` JSON,-- end editing
  PRIMARY KEY (`bio_id`),
  CONSTRAINT `tcga_case_id_in_bio` FOREIGN KEY (`tcga_case_id`) REFERENCES `patient` (`tcga_case_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;

CREATE TABLE `wsi` (
  `slide_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `tcga_wsi_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `tcga_case_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `tcga_wsi_slide_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
#   `bio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`slide_id`),
  CONSTRAINT `tcga_case_id_in_wsi` FOREIGN KEY (`tcga_case_id`) REFERENCES `patient` (`tcga_case_id`) ON DELETE CASCADE ON UPDATE CASCADE
#   CONSTRAINT `bio_id_in_wsi` FOREIGN KEY (`bio_id`) REFERENCES `biospecimen` (`bio_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

# -- Insert mock-up wsi list with a loop
# drop procedure if exists load_foo_test_data;
#
# USE `db_wsi4_dev`;
# delimiter #
# create procedure load_foo_test_data()
# begin
#
# declare v_max int unsigned default 1000;
# declare v_counter int unsigned default 0;
#   while v_counter < v_max do
#     INSERT INTO `db_wsi4_dev`.`wsi` (`tcga_wsi_id`, `tcga_case_id`, `uuid`) VALUES ('123456', '123', '123');
#     set v_counter=v_counter+1;
#   end while;
#   commit;
# end #
#
# delimiter ;
#
# call load_foo_test_data();


CREATE TABLE `grading` (
  `grading_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `grading_std_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NA',
  PRIMARY KEY (`grading_id`)
) ENGINE=InnoDB ;

-- Insert ISUP grading system
INSERT INTO `db_wsi4_dev`.`grading` (`grading_std_name`) VALUES ('ISUP1');
INSERT INTO `db_wsi4_dev`.`grading` (`grading_std_name`) VALUES ('ISUP2');
INSERT INTO `db_wsi4_dev`.`grading` (`grading_std_name`) VALUES ('ISUP3');
INSERT INTO `db_wsi4_dev`.`grading` (`grading_std_name`) VALUES ('ISUP4');

CREATE TABLE `pslv` (
  `pslv_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `subregion_density` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pslv_id`)
) ENGINE=InnoDB ;

-- Insert sub-region density
INSERT INTO `db_wsi4_dev`.`pslv` (`subregion_density`) VALUES (5000);
INSERT INTO `db_wsi4_dev`.`pslv` (`subregion_density`) VALUES (1000);
INSERT INTO `db_wsi4_dev`.`pslv` (`subregion_density`) VALUES (60);

CREATE TABLE `point` (
  `pt_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned DEFAULT NULL,
  `annotation_ts` datetime NOT NULL,  -- Annotation timestamp
  `grading_id` tinyint(3) unsigned NOT NULL,
  `slide_id` smallint(6) unsigned NOT NULL,
  `annotator_id` smallint(5) unsigned NOT NULL,
  `pslv_id` tinyint(3) unsigned DEFAULT NULL,
  -- keep adding code
  `region_id` int(10) unsigned NOT NULL,
  `selected` tinyint(2) unsigned NOT NULL,
  `anno_batch` int(10) unsigned NOT NULL,
  -- end editing
  PRIMARY KEY (`pt_id`),
  KEY `INDEX` (`x`,`y`) USING BTREE,
  KEY `annotator_id_idx` (`annotator_id`),
  KEY `slide_id_idx` (`slide_id`),
  KEY `grading_id_idx` (`grading_id`),
  KEY `pslv_id_idx` (`pslv_id`),
  CONSTRAINT `annotator_id` FOREIGN KEY (`annotator_id`) REFERENCES `annotator` (`annotator_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `grading_id` FOREIGN KEY (`grading_id`) REFERENCES `grading` (`grading_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pslv_id` FOREIGN KEY (`pslv_id`) REFERENCES `pslv` (`pslv_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `slide_id` FOREIGN KEY (`slide_id`) REFERENCES `wsi` (`slide_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;

-- Insert annotator password
INSERT INTO annotator(password) values ('12345')
