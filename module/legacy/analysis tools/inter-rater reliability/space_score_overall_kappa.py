import cv2
import numpy as np


def compute_space_score_kappa(img1, img2):
    """
    This is to compute the space score between two images using Kappa statics as the metrics.
    :param img1: first image
    :param img2: second image
    :return: overall kappa score
    """
    img1gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    ret1, mask1 = cv2.threshold(img1gray, 10, 255, cv2.THRESH_BINARY)
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret2, mask2 = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    and_mask1_mask2 = cv2.bitwise_and(mask1, mask2)
    and_nzCount = cv2.countNonZero(and_mask1_mask2)
    num11 = and_nzCount
    image_one_zero = cv2.subtract(mask1, and_mask1_mask2)
    num10 = cv2.countNonZero(image_one_zero)
    image_zero_one = cv2.subtract(mask2, and_mask1_mask2)
    num01 = cv2.countNonZero(image_zero_one)
    image_zero_zero = cv2.bitwise_not(cv2.bitwise_or(mask1, mask2))
    num00 = cv2.countNonZero(image_zero_zero)
    cm1 = num00 + num10
    cm2 = num01 + num11
    rm1 = num00 + num10
    rm2 = num10 + num11
    n = cm1 + cm2
    pra = (num00 + num11) / n
    pre = ((cm1 * rm1) / n + (cm2 * rm2) / n) / n
    kappa = (pra - pre) / (1 - pre)
    SEk = np.sqrt(pra * (1 - pra) / (n * np.square(1 - pre)))
    minKappa = kappa - 1.96 * SEk
    maxKappa = kappa + 1.96 * SEk
    return kappa, minKappa, maxKappa


if __name__ == '__main__':
    img1 = cv2.imread('data/anno_a1s1p0.png')
    img2 = cv2.imread('data/anno_a2s1p0.png')
    kappa, min, max = compute_space_score_kappa(img1, img2)
    print(kappa, min, max)
