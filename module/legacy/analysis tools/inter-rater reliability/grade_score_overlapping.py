import cv2
import numpy as np


def compute_grade_score_overlapping(image1, image2):
    """
    This is to compute the grade score between two images, only considering the overlapping part.
    :param image1: first image
    :param image2: second image
    :return: grade score
    """
    img1 = cv2.imread(image1)
    img2 = cv2.imread(image2)
    bitwiseand = cv2.bitwise_and(img1, img2)
    green_green = 0    # grade0 - grade0
    yellow_yellow = 0  # grade1 - grade1
    green_yellow = 0   # grade0 - grade1
    for i in range(img1.shape[0]):
        for j in range(img1.shape[1]):
            if bitwiseand[i][j][0] == 124 and bitwiseand[i][j][1] == 252 and bitwiseand[i][j][2] == 0:
                green_green += 1
            elif bitwiseand[i][j][0] == 0 and bitwiseand[i][j][1] == 255 and bitwiseand[i][j][2] == 255:
                yellow_yellow += 1
            elif bitwiseand[i][j][0] == 0 and bitwiseand[i][j][1] == 252 and bitwiseand[i][j][2] == 0:
                green_yellow += 1
    matrix = np.zeros((4, 4))
    matrix[1][1] = green_green * 2
    matrix[2][2] = yellow_yellow * 2
    matrix[1][2] = matrix[2][1] = green_yellow
    n1, n2, n3, n4 = np.sum(matrix, axis=0)
    n = n1 + n2 + n3 + n4
    tmp = n1 * (n1 - 1) + n2 * (n2 - 1) + n3 * (n3 - 1) + n4 * (n4 - 1)
    tmp1 = (n - 1) * (matrix[0][0] + matrix[1][1] + matrix[2][2] + matrix[3][3])
    alpha = (tmp1 - tmp) / (n * (n - 1) - tmp)
    return alpha


if __name__ == '__main__':
    score = compute_grade_score_overlapping('data/anno_a1s1p6.png','data/anno_a2s1p6.png')
    print(score)
