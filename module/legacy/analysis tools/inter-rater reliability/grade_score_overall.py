import cv2
import numpy as np


def compute_grade_score_overall(img1, img2):
    """
    This is to compute the grade score（overall） between two images.
    :param img1: first image
    :param img2: second image
    :return: overall grade score
    """
    matrix = np.zeros((4, 4))
    for i in range(img1.shape[0]):
        for j in range(img1.shape[1]):
            if np.sum(img1[i][j]) == 0 and np.sum(img2[i][j]) == 0:
                matrix[0][0] += 2
            elif (np.sum(img1[i][j]) == 124+252+0 and np.sum(img2[i][j] == 0)) or (np.sum(img2[i][j]) == 124+252+0 and np.sum(img1[i][j]) ==0 ):
                matrix[0][1] += 1
                matrix[1][0] += 1
            elif( (np.sum(img1[i][j]) == 0) and np.sum(img2[i][j] == 255+255+0)) or ((np.sum(img2[i][j]) == 0) and np.sum(img1[i][j] == 255+255+0) ):
                matrix[0][2] += 1
                matrix[2][0] += 1
            elif (np.sum(img1[i][j]) == 124+252+0) and (np.sum(img2[i][j]) == 124+252+0):
                matrix[1][1] += 2
            elif (np.sum(img1[i][j]) == 255+255+0) and (np.sum(img2[i][j]) == 255+255+0):
                matrix[2][2] += 2

    n1, n2, n3, n4 = np.sum(matrix, axis=0)
    n = img1.shape[0] * img1.shape[1]
    tmp = n1 * (n1-1) + n2 * (n2-1) + n3 * (n3-1) + n4 * (n4-1)
    alpha = ((n-1) * (matrix[0][0] + matrix[1][1] + matrix[2][2] + matrix[3][3]) - tmp) / (n * (n-1) - tmp)
    return alpha


if __name__ == 'main':
    img1 = cv2.imread('data/anno_a1s1p0.png')
    img2 = cv2.imread('data/anno_a2s1p0.png')
    alpha = compute_grade_score_overall(img1, img2)
    print(alpha)

