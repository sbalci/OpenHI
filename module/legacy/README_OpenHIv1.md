# OpenHIv1 - Open Histopathological Image version 1

### Note about depreciation
The code on this branch (OpenHIv1) `v1_support` will be depreciated and replaced with the second version of OpenHI (OpenHIv2). 

## How to cite OpenHI
Puttapirat Pargorn, Haichuan Zhang, Yuchen Lian, Chunbao Wang, Xiangrong Zhang, Lixia Yao, Chen Li, "OpenHI - An open source framework for annotating histopathological image," 2018 IEEE International Conference on Bioinformatics and Biomedicine (BIBM), Madrid, Spain, 2018, pp. 1076-1082.

doi: 10.1109/BIBM.2018.8621393 <br /> 
[IEEE URL](https://ieeexplore.ieee.org/document/8621393) or [IEEE PDF](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8621393&isnumber=8621069)

### Overview
OpenHI is an open source framework for annotating histopathological image. 
OpenHI can: 
1. Annotate with different level of gradings. For example (G1, G2, G3) or (0, 1)
2. Provide different sizes of pre-defined segmentation segments with superpixel segmentation algorithm .
3. Fluidly view the whole-slide image from different vendors. Thanks to [OpenSlide](https://openslide.org)
4. Provide option to create a list of focused region. 
5. Provide accompanying slide information parsed from GDC XML format. 

#### OpenHI in action
GUI: 
![OpenHI Sample GUI](../../docs/source/legacy/gui_sample_1.png) 

# How to install and use OpenHI
We have extensively tested OpenHI on Linux environment. However, it is possible 
to run OpenHI on Windows and Mac where the following dependencies are supported.

1. Prepare your machine to run the framework, make sure that the minimal 
specification meets the requirement. 
2. Make sure that you have working MySQL server. 
3. Install all the dependencies (python packages) (See [*Software dependencies*](#software-dependencies))
4. Build the MySQL table using script in _**/sql/**_ folder. The current version is `data_model_4-1`. (See [MySQL config](#mysql-database-configuration))

For detail, you can read [readme_install](https://gitlab.com/BioAI/OpenHI/blob/dev_BioAI-team/documentation/Readme_install.md)

## Hardware requirements
* Multi-core CPU
* At least 8 GB of memory for OpenHI web framework, and 128GB+ for image pre-processing module. 
* Enough disk space to accommodate the WSI files. 

## Software dependencies
* Clone the repository.
* Setup MySQL server.
* Copy data model from BioTM2 Server (192.168.30.160).
* Configure the 'proc_mysql.py' to accept the new local server.
* Install OpenSlide: [OpenSlide library on the machine](install-openslide)
* Install necessary python packages using 'pip install -r requirements.txt'
  
## OpenHI configuration
The OpenHI has one main external configuration file which includes the following configurations. 
* MySQL setting
  * Host and port
  * Authentication (username and password)
  * OpenHI database schema name (include to support different data models)
* Web framework configuration
  * Host and port [default at 0.0.0.0 port 5000]
  * Running modes
* WSI
  * Default WSI for newly logged-in annotators.

## Running the framework
### MySQL database configuration
**Database version:** v4.1
* Add the MySQL server configuration to the configuration file located in `static/data_model_4-1_conf.ini`. Noted that
this file can be based on the example file located at `static/data_model_4-1_conf_example.ini`.
* Run the MySQL script to create the data schema in your MySQL server. Run: `static/data_model_4-1.sql`
* Run the Python file to fill the newly created table with the clinical and biospecimen data (xml file) using python 
script. Run `static/data_model_4-1_fill_info.py`.

### Start the OpenHI Framework with Flask
To run the framework run the main Flask python file which is `app.py` with the following command. 
```
python app.py
```
Try to visit the GUI using a web browser at the specified port. 

# Project organization
| Name | Description |
| ---- | --- | 
| app.py | This is the main script with supporting scripts in the files that starts with ***anno_*** | 
| anno_* | The scripts starting with ***anno_*** is the bit of code that will be used in the project. Each aspect of the code is written in different files. | 
| dev_* | The scripts starting with ***dev_*** is for development and try outs only. 
| (folder) documentation | This folder contains all the documentation of the software |
| (folder) framework_src | The framework source file contains the necessary WSI files to run. | 
| (folder) mysql | All the MySQL scripts used in this project will be stored here for future references |
| (folder/Flask framework) static *and* templates | This is part of the standard Flask web framework which need **static** folder to store files of the frameowkr and **templates** folder to save all the HTML templates in the framework. | 
| (folder) img_preproc_matlab | Image pre-processing code written in *MATLAB* of OpenHI | 
| (folder) img_preproc_opensource | Image pre-processing code written in Python of OpenHI | 