"""
The dbv1 or "database version 1" is the bridge to save WSI boundaries as organized file structure.
This version is designed based on TCGA WSI data structure. Version 2 will be more explicit and easier to work with.
"""
import json
import os

# Load configuration JSON file
with open('/home1/pp/docs/py-proj/flask-anno2/img_preproc_opensource_opensource/tcga_connector_conf.json') as json_data_file:
    conf = json.load(json_data_file)


def get_boundary_filename(slide_number, superpixel_density):
    """
    This function will return fullpath of binary boundary matrix file according to the given slide number and
    superpixel segmentation density.
    :param slide_number: destination slide number
    :param superpixel_density: sub-region density of the binary boundary.
    :return: fullpath + appropriate filename, without image format extension (e.g. ".jpg", ".png", or ".tiff")
    """
    # Load manifest file
    # (note) !! Manifest file is always named 'manifest2.txt'
    fn_mani = conf['fullpath-manifest']

    # Load manifest
    file_stream = open(fn_mani, "r")

    num_of_file_counter = 0
    # Get and display header of the file (not used)
    mani_header = file_stream.readline()

    # Prepare the parameters
    uuid = []
    fn_wsi = []
    md5 = []
    size = []
    state = []

    # Run through the manifest file line-by-line to acquire all information necessary.
    for line in file_stream:
        loop_line = line
        # print(len(loop_line))
        loop_a = loop_line.split("\t")
        # print(loop_line.split("\t"))

        uuid.append(loop_a[0])
        fn_wsi.append(loop_a[1])
        md5.append(loop_a[2])
        size.append(loop_a[3])
        state.append(loop_a[4])

        num_of_file_counter += 1

    # result: uuid, fn_wsi, size (Note: at this line, the 3 variable is what we get from reading manifest2.txt)

    # ----- File name organizing area ----- #
    fn_len = len(fn_wsi[slide_number]) - 4  # Select only the filename, excluding file extension.
    filesep = os.sep

    # Creating full bwboundary path for wsi
    boun_filename = (
            conf['fullpath-parent'] + filesep +
            uuid[slide_number] + filesep +
            fn_wsi[slide_number][:fn_len] + '_super' +
            str(superpixel_density)
    )

    return boun_filename


if __name__ == '__main__':
    print('This script is not meant to be run as a main function. Please refer to the documentation')


