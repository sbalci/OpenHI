import time
import sys


def progress(count, total, suffix=''):
    """
    by Andrea Griffini (https://stackoverflow.com/a/6169274)
    :param count: the number of iteration
    :param total: max iteration
    :param suffix: suffix
    :return: progress bar as text-output
    """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben


for i in range(100):
    time.sleep(0.2)
    progress(i, 100)

